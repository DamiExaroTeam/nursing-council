﻿using Nursing_Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Nursing_Contract
{
    [ServiceContract]
    public interface INursing
    {
        [OperationContract]
        List<NursingReport> GetLastYear();

        [OperationContract]
        List<NursingReport> GetThisYear();

        [OperationContract]
        Dictionary<DateTime, int> GetReceivedThisYear();

        [OperationContract]
        List<NursingReport> GetLastTenRegistered();

        [OperationContract]
        Dictionary<string, int> GetOperationsStats();

        [OperationContract]
        Dictionary<string, Dictionary<string, int>> GetOperationAverages(DateTime start, DateTime end);

        [OperationContract]
        Dictionary<string, int> GetNursingRegistrationsData(DateTime start, DateTime end, int type);
    }
}
