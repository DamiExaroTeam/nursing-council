﻿using Nursing_Common;
using Nursing_Contract;
using Nursing_Data;
using System;
using System.Collections.Generic;

namespace Nursing_Service
{
    public class NursingService : INursing
    {
        public List<NursingReport> GetLastYear()
        {
            return new NursingData().GetLastYear();
        }

        public List<NursingReport> GetThisYear()
        {
            return new NursingData().GetThisYear();
        }

        public Dictionary<DateTime, int> GetReceivedThisYear()
        {
            return new NursingData().GetReceivedThisYear();
        }

        public List<NursingReport> GetLastTenRegistered()
        {
            return new NursingData().GetLastTenRegistered();
        }

        public Dictionary<string, int> GetOperationsStats()
        {
            return new NursingData().GetOperationsStats();
        }

        public Dictionary<string, Dictionary<string, int>> GetOperationAverages(DateTime start, DateTime end)
        {
            return new NursingData().GetOperationAverages(start, end);
        }

        public Dictionary<string, int> GetNursingRegistrationsData(DateTime start, DateTime end, int type)
        {
            return new NursingData().GetNursingRegistrationsData(start, end, type);
        }
    }
}
