﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Nursing_Data
{
    public class DataProvider
    {
        public SqlConnection Connection;
        public SqlCommand Command;
        public string ConnectionString;

        public DataProvider(string conn)
        {
            ConnectionString = conn;
        }

        public void CreateConnection()
        {
            if (Connection != null)
                return;
            Connection = new SqlConnection(ConnectionString);
        }

        public int ExecuteQuery(IDataParameter[] parameters)
        {
            int num;
            SqlCommand command = Command;
            try
            {
                if (command.Connection.State == ConnectionState.Closed)
                    command.Connection.Open();
                if (parameters != null && (uint)parameters.Length > 0U)
                {
                    command.Parameters.Clear();
                    foreach (var p in parameters)
                    {
                        if (p != null)
                            command.Parameters.Add(p);
                    }
                }
                num = command.ExecuteNonQuery();
            }
            finally
            {
                Connection?.Close();
            }
            return num;
        }

        public int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int num;
            try
            {
                if (Connection == null)
                    CreateConnection();
                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
                using (SqlCommand command = Connection.CreateCommand())
                {
                    command.CommandText = strQuery;
                    if (parameters != null && (uint)parameters.Length > 0U)
                    {
                        for (int index = 0; index < parameters.Length; ++index)
                        {
                            if (parameters[index].Value == null)
                                parameters[index].Value = DBNull.Value;
                            command.Parameters.Add(parameters[index]);
                        }
                    }
                    command.Connection = Connection;
                    num = command.ExecuteNonQuery();
                }
            }
            finally
            {
                Connection?.Close();
            }
            return num;
        }

        public int ExecuteScalar(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected;

            try
            {
                if (Connection == null)
                {
                    CreateConnection();
                }

                if (Connection.State != ConnectionState.Open)
                    Connection.Open();

                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = Connection;
                    iRowsAffected = (int)cmd.ExecuteScalar();
                }
            }
            finally
            {
                Connection?.Close();
            }
            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, SqlParameter[] parameters, DataSet dataSet)
        {
            try
            {
                if (Connection == null)
                    CreateConnection();
                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
                using (SqlCommand command = Connection.CreateCommand())
                {
                    command.CommandText = strQuery;
                    if (parameters != null && (uint)parameters.Length > 0U)
                    {
                        for (int index = 0; index < parameters.Length; ++index)
                            command.Parameters.Add(parameters[index]);
                    }
                    using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                    {
                        sqlDataAdapter.SelectCommand = command;
                        sqlDataAdapter.Fill(dataSet);
                        Connection?.Close();
                    }
                }
            }
            finally
            {
                if (Connection?.State == ConnectionState.Open)
                    Connection?.Close();
            }
        }

        public void CloseQuery()
        {
            Connection?.Close();
        }

        public int GetLastInsert(string tablename)
        {
            try
            {
                if (Connection == null)
                    CreateConnection();
                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
                using (var command = Connection.CreateCommand())
                {
                    command.CommandText = "SELECT IDENT_CURRENT('" + tablename + "')";
                    command.Connection = Connection;
                    return Convert.ToInt32(command.ExecuteScalar());
                }
            }
            finally
            {
                Connection?.Close();
            }
        }
    }
}
