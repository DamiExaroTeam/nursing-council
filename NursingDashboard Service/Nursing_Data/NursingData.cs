﻿using Core;
using Nursing_Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Nursing_Data
{
    public class NursingData
    {
        public List<NursingReport> GetThisYear()
        {
            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var nursingReportList = new List<NursingReport>();
            using (var dataSet = new DataSet())
            {
                var start = new DateTime(DateTime.Today.Year, 1, 1);
                var end = new DateTime(DateTime.Today.Year + 1, 1, 1);

                var strQuery = $@"select print_request_date, qcdate, printdate 
                                from NursingPrintRequest n left join cardprintlog c on c.cardid = n.Print_Request_Sequence
	                            left join batch b on b.batchid = n.batch_id
                                where (sysregdate is null and print_request_date >= @start and print_request_date < @end) or (sysregdate is not null and sysregdate >= @start and sysregdate < @end)";

                var param = new List<SqlParameter>();
                param.Add(new SqlParameter()
                {
                    DbType = DbType.DateTime,
                    ParameterName = "start",
                    Value = start
                });

                param.Add(new SqlParameter()
                {
                    DbType = DbType.DateTime,
                    ParameterName = "end",
                    Value = end
                });

                dataProvider.RunProcedure(strQuery, param.ToArray(), dataSet);

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    nursingReportList.Add(new NursingReport()
                    {
                        DateCaptured = string.IsNullOrEmpty(row["print_request_date"].ToString()) ? DateTime.MinValue : DateTime.Parse(row["print_request_date"].ToString()),
                        DateQCd = string.IsNullOrEmpty(row["qcdate"].ToString()) ? DateTime.MinValue : DateTime.Parse(row["qcdate"].ToString()),
                        PrintDate = string.IsNullOrEmpty(row["printdate"].ToString()) ? DateTime.MinValue : DateTime.Parse(row["printdate"].ToString()),
                    });
                }
            }

            return nursingReportList;
        }

        public Dictionary<DateTime, int> GetReceivedThisYear()
        {
            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var dic = new Dictionary<DateTime, int>();

            using (var dataSet = new DataSet())
            {
                var start = new DateTime(DateTime.Today.Year, 1, 1);
                var end = new DateTime(DateTime.Today.Year + 1, 1, 1);

                var strQuery = $@"select sum(applications) as applications, CAST(sysregdate AS DATE) as sysregdate from batch
                                where sysregdate  >= @start and sysregdate <= @end group by CAST(sysregdate AS DATE)
                                order by sysregdate";

                var param = new List<SqlParameter>();
                param.Add(new SqlParameter()
                {
                    DbType = DbType.DateTime,
                    ParameterName = "start",
                    Value = start
                });

                param.Add(new SqlParameter()
                {
                    DbType = DbType.DateTime,
                    ParameterName = "end",
                    Value = end
                });

                dataProvider.RunProcedure(strQuery, param.ToArray(), dataSet);

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                     dic.Add(DateTime.Parse(row["sysregdate"].ToString()), int.Parse(row["applications"].ToString()));
                }
            }

            return dic;
        }

        public List<NursingReport> GetLastYear()
        {
            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var nursingReportList = new List<NursingReport>();
            using (var dataSet = new DataSet())
            {
                var strQuery = "select CardPrintLogId, Printdate from CardPrintLog where printdate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)";
                dataProvider.RunProcedure(strQuery, null, dataSet);
                foreach (DataRow row in dataSet.Tables[0].Rows)
                    nursingReportList.Add(new NursingReport()
                    {
                        PrintDate = DateTime.Parse(row["Printdate"].ToString()),
                        CardId = int.Parse(row["CardPrintLogId"].ToString())
                    });
            }
            return nursingReportList;
        }

        public List<NursingReport> GetLastYearOperations()
        {
            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var nursingReportList = new List<NursingReport>();
            using (var dataSet = new DataSet())
            {
                var strQuery = "select CardPrintLogId, Printdate from CardPrintLog where printdate >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0)";
                dataProvider.RunProcedure(strQuery, null, dataSet);
                foreach (DataRow row in dataSet.Tables[0].Rows)
                    nursingReportList.Add(new NursingReport()
                    {
                        PrintDate = DateTime.Parse(row["Printdate"].ToString()),
                        CardId = int.Parse(row["CardPrintLogId"].ToString())
                    });
            }
            return nursingReportList;
        }

        public List<NursingReport> GetLastTenRegistered()
        {
            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var nursingReportList = new List<NursingReport>();
            using (var dataSet = new DataSet())
            {
                var strQuery = @"select Top 10 first_name, surname, date_of_birth, middle_name, gender, photo, primary_specialisation, CardId, Printdate
                                    from CardPrintLog c left join NursingPrintRequest n on c.CardID = n.Print_Request_Sequence
                                    order by CardPrintLogId desc";
                dataProvider.RunProcedure(strQuery, null, dataSet);
                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    nursingReportList.Add(new NursingReport()
                    {
                        PrintDate = DateTime.Parse(row["Printdate"].ToString()),
                        Dob = DateTime.Parse(row["date_of_birth"].ToString()),
                        CardId = int.Parse(row["CardId"].ToString()),
                        Firstname = row["first_name"].ToString(),
                        Surname = row["surname"].ToString(),
                        Gender = row["gender"].ToString() == "M" ? "Male" : "Female",
                        Middename = row["middle_name"].ToString(),
                        Pri = row["primary_specialisation"].ToString(),
                        Photo = row["photo"] as byte[]
                    });
                }
            }

            return nursingReportList;
        }

        public Dictionary<string, Dictionary<string, int>> GetOperationAverages(DateTime start, DateTime end)
        {
            var result = new Dictionary<string, Dictionary<string, int>>();

            var query = $@"select sysregdate, print_request_date, qcdate, printdate 
                            from NursingPrintRequest n 
                            left join cardprintlog c on c.cardid = n.Print_Request_Sequence
                            left join batch b on b.batchid = n.batch_id
                            where print_request_date >= @start and print_request_date <= @end";

            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var capture = new Dictionary<string, List<double>>();
            var qc = new Dictionary<string, List<double>>();
            var print = new Dictionary<string, List<double>>();
            var total = new Dictionary<string, List<double>>();

            using (var dataSet = new DataSet())
            {
                var param = new List<SqlParameter>();
                param.Add(new SqlParameter()
                {
                    DbType = DbType.DateTime,
                    ParameterName = "start",
                    Value = start
                });

                param.Add(new SqlParameter()
                {
                    DbType = DbType.DateTime,
                    ParameterName = "end",
                    Value = end
                });

                dataProvider.RunProcedure(query, param.ToArray(), dataSet);

                foreach (DataRow row in dataSet.Tables[0].Rows)
                {
                    var calc = new Calculation(new List<DateTime>(), new OpenHours("09:00;16:00"));

                    if (!string.IsNullOrEmpty(row["sysregdate"].ToString()) && !string.IsNullOrEmpty(row["print_request_date"].ToString()))
                    {
                        var receivedtime = DateTime.Parse(row["sysregdate"].ToString());
                        var capturetime = DateTime.Parse(row["print_request_date"].ToString());

                        // For some reason we have forms which the capture time is years before the recieved time
                        if (capturetime > receivedtime)
                        {
                            if (!capture.ContainsKey(capturetime.ToString("MMM yy")))
                                capture.Add(capturetime.ToString("MMM yy"), new List<double>());

                            capture[capturetime.ToString("MMM yy")].Add(calc.getElapsedMinutes(receivedtime, capturetime) / 60);
                        }
                    }

                    if (!string.IsNullOrEmpty(row["print_request_date"].ToString()) && !string.IsNullOrEmpty(row["qcdate"].ToString()))
                    {
                        var capturetime = DateTime.Parse(row["print_request_date"].ToString());
                        var qctime = DateTime.Parse(row["qcdate"].ToString());

                        if (!qc.ContainsKey(qctime.ToString("MMM yy")))
                            qc.Add(qctime.ToString("MMM yy"), new List<double>());
                            
                        qc[qctime.ToString("MMM yy")].Add(calc.getElapsedMinutes(capturetime, qctime) / 60);
                    }

                    if (!string.IsNullOrEmpty(row["qcdate"].ToString()) && !string.IsNullOrEmpty(row["printdate"].ToString()))
                    {
                        var qctime = DateTime.Parse(row["qcdate"].ToString());
                        var printtime = DateTime.Parse(row["printdate"].ToString());

                        if (!print.ContainsKey(printtime.ToString("MMM yy")))
                            print.Add(printtime.ToString("MMM yy"), new List<double>());
                        print[printtime.ToString("MMM yy")].Add(calc.getElapsedMinutes(qctime, printtime) / 60);
                    }

                    if (!string.IsNullOrEmpty(row["sysregdate"].ToString()) && !string.IsNullOrEmpty(row["printdate"].ToString()) && !string.IsNullOrEmpty(row["print_request_date"].ToString()))
                    {
                        var receivedtime = DateTime.Parse(row["sysregdate"].ToString());
                        var printtime = DateTime.Parse(row["printdate"].ToString());
                        var capturetime = DateTime.Parse(row["print_request_date"].ToString());

                        if (capturetime > receivedtime)
                        {
                            if (!total.ContainsKey(printtime.ToString("MMM yy")))
                                total.Add(printtime.ToString("MMM yy"), new List<double>());

                            total[printtime.ToString("MMM yy")].Add(calc.getElapsedMinutes(receivedtime, printtime) / 60);
                        }
                    }
                }

                // convert to average per month
                var captureavg = new Dictionary<string, int>();

                var monthlist = new List<string>();
                var format = "MMM yy";

                for (var d = start; d <= end; d = d.AddMonths(1))
                {
                    monthlist.Add(d.ToString(format));
                }

                foreach (var month in monthlist)
                {
                    if (!captureavg.ContainsKey(month))
                        captureavg.Add(month, 0);

                    if (!capture.ContainsKey(month))
                        captureavg[month] = 0;
                    else
                        captureavg[month] = Convert.ToInt32(capture[month].DefaultIfEmpty(0).Average());
                }

                var qcavg = new Dictionary<string, int>();
                foreach (var month in monthlist)
                {
                    if (!qcavg.ContainsKey(month))
                        qcavg.Add(month, 0);

                    if (!qc.ContainsKey(month))
                        qcavg[month] = 0;
                    else
                        qcavg[month] = Convert.ToInt32(qc[month].DefaultIfEmpty(0).Average());
                }

                var printavg = new Dictionary<string, int>();
                foreach (var month in monthlist)
                {
                    if (!printavg.ContainsKey(month))
                        printavg.Add(month, 0);

                    if (!print.ContainsKey(month))
                        printavg[month] = 0;
                    else
                        printavg[month] = Convert.ToInt32(print[month].DefaultIfEmpty(0).Average());
                }

                var totalavg = new Dictionary<string, int>();
                foreach (var month in monthlist)
                {
                    if (!totalavg.ContainsKey(month))
                        totalavg.Add(month, 0);

                    if (!total.ContainsKey(month))
                        totalavg[month] = 0;
                    else
                        totalavg[month] = Convert.ToInt32(total[month].DefaultIfEmpty(0).Average());
                }

                result.Add("qcavg", qcavg);
                result.Add("captureavg", captureavg);
                result.Add("printavg", printavg);
                result.Add("totalavg", totalavg);

                return result;
            }
        }

        public Dictionary<string, int> GetOperationsStats()
        {
            // Get total awaiting Capture
            var query = @"select (sum(applications) - sum(captured)) as awaitingcapture from [CardPrintData].[dbo].[batch] where batchid != 2005 and batchid != 2016";
            var dataProvider = new DataProvider(ConfigurationManager.ConnectionStrings["NursingConnection"].ToString());
            var awaitingcapture = dataProvider.ExecuteScalar(query, null);

            // Get total awaiting QC
            query = @"select count(print_request_sequence) from [CardPrintData].[dbo].[NursingPrintRequest] where status = 0 and print_request_date >= '2017-01-01'";
            var awaitingqc = dataProvider.ExecuteScalar(query, null);

            // Get total awaiting Print
            query = @"select count(print_request_sequence) from [CardPrintData].[dbo].[NursingPrintRequest] where (status = 1 or status = 4) and print_request_date >= '2017-01-01'";
            var awaitingprint = dataProvider.ExecuteScalar(query, null);

            var result = new Dictionary<string, int>();
            result.Add("qc", awaitingqc);
            result.Add("print", awaitingprint);
            result.Add("capture", awaitingcapture);

            return result;
        }

        public Dictionary<string, int> GetNursingRegistrationsData(DateTime start, DateTime end, int type)
        {
            var dateTime1 = start;
            var dateTime2 = end;
            var dictionary1 = new Dictionary<string, int>();
            DateTime dateTime3;
            if (type == 1)
            {
                dateTime1 = new DateTime(start.Year, start.Month, 1);
                dateTime3 = dateTime1.AddMonths(1);
                dateTime2 = dateTime3.AddDays(-1.0);
                for (var index = 1; index < DateTime.DaysInMonth(start.Year, start.Month) + 1; ++index)
                {
                    dictionary1.Add(index.ToString(), 0);
                }
            }
            if (type == 2)
            {
                dateTime1 = new DateTime(start.Year, start.Month, 1);
                var stringList1 = Helper.MonthsBetween(start, end).Select(x => x.Item1).ToList();
                foreach (var key in stringList1)
                    dictionary1.Add(key, 0);
            }
            var cmdText = @"select trunc(registration_date, 'Mon') as registration_date, count(m.member_id) as count
                                from member m, MOBILE_DEVICE MD WHERE M.fk_device_enroll = MD.MD_ID AND MD.FK_PROGRAM_OWNER = 2 AND M.LKP_MEMBER_TYPE = 143
                                and registration_date >= to_date('" + dateTime1.ToString("MMM-yyyy") + @"', 'Mon-YYYY') 
                                and registration_date <= to_date('" + dateTime2.ToString("MMM-yyyy") + @"', 'Mon-YYYY')
                                group by trunc(registration_date, 'Mon')
                                order by trunc(registration_date, 'Mon'), count desc";
            if (type == 0 || type == 1)
            {
                cmdText = @"select trunc(registration_date) as registration_date, count(m.member_id) as count
                            from member m, MOBILE_DEVICE MD
                            WHERE M.fk_device_enroll = MD.MD_ID AND MD.FK_PROGRAM_OWNER = 2 AND M.LKP_MEMBER_TYPE = 143
                            and registration_date >= to_date('" + dateTime1.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS')
                            and registration_date <= to_date('" + dateTime2.ToString("dd-MMM-yyyy") + @" 23:59:59', 'DD-Mon-YYYY HH24:MI:SS')
                            group by trunc(registration_date) order by trunc(registration_date), count desc";

                if (dictionary1.Count == 0)
                {
                    for (var index = 1; index < DateTime.DaysInMonth(start.Year, start.Month) + 1; ++index)
                        dictionary1.Add(index.ToString(), 0);
                }
            }

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicationConnection"].ToString()))
            {
                using (var sqlCommand = new SqlCommand(cmdText, connection))
                {
                    connection.Open();
                    using (var sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (sqlDataReader.Read())
                        {
                            string str;
                            if (type == 2)
                            {
                                dateTime3 = DateTime.Parse(sqlDataReader["registration_date"].ToString());
                                str = dateTime3.ToString("MMM yy");
                            }
                            else
                            {
                                dateTime3 = DateTime.Parse(sqlDataReader["registration_date"].ToString());
                                str = dateTime3.Day.ToString();
                            }
                            var num = int.Parse(sqlDataReader["count"].ToString());
                            var dictionary3 = dictionary1;
                            var index = str;
                            dictionary3[index] = dictionary3[index] + num;
                        }
                    }
                }
            }

            return dictionary1;
        }

        private int GetNumberOfHoursExcludingWkends(DateTime start, DateTime end)
        {
            var dt3 = end - start;

            var hourCount = 0;
            var totalHours = 0;

            totalHours += dt3.Days * 24;
            totalHours += dt3.Hours;

            //1 hour timespan
            var hour = new TimeSpan(1, 0, 0);

            for (var i = 1; i < totalHours + 1; i++)
            {
                end = end.Subtract(hour);

                if (end.DayOfWeek != DayOfWeek.Saturday && end.DayOfWeek != DayOfWeek.Sunday)
                {
                    hourCount++;
                }
            }

            return hourCount;
        }

        public static DateTime Round(DateTime dateTime)
        {
            var updated = dateTime.AddMinutes(30);
            return new DateTime(updated.Year, updated.Month, updated.Day,
                                 updated.Hour, 0, 0, dateTime.Kind);
        }
    }
}
