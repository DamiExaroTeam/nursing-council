﻿using Nursing_Contract;
using System;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Nursing_Proxy
{
    public class NursingProxy
    {
        private ChannelFactory<INursing> cf = (ChannelFactory<INursing>)null;

        public INursing NursingChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public NursingProxy()
        {
            this.ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"].ToString() + "/NursingService.svc";
            WSHttpBinding wsHttpBinding = new WSHttpBinding();
            wsHttpBinding.MaxReceivedMessageSize = 4194304L;
            wsHttpBinding.Security.Mode = SecurityMode.Transport;
            wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            wsHttpBinding.SendTimeout = new TimeSpan(0, 10, 0);
            wsHttpBinding.ReceiveTimeout = new TimeSpan(0, 10, 0);
            wsHttpBinding.MaxReceivedMessageSize = (long)int.MaxValue;
            wsHttpBinding.ReaderQuotas.MaxArrayLength = 4194304;
            ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)((param0, param1, param2, param3) => true);
            this.cf = new ChannelFactory<INursing>((Binding)wsHttpBinding, this.ServiceBaseAddress);
            this.NursingChannel = this.cf.CreateChannel();
        }
    }
}
