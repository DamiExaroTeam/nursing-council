﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NursingDashboard.Models
{
    public class DataVals
    {
        public int ReceivedToday { get; set; }
        public int ReceivedThisWeek { get; set; }
        public int ReceivedThisMonthy { get; set; }
        public int ReceivedThisYear { get; set; }

        public int CapturedToday { get; set; }
        public int CapturedThisWeek { get; set; }
        public int CapturedThisMonthy { get; set; }
        public int CapturedThisYear { get; set; }

        public int QCToday { get; set; }
        public int QCThisWeek { get; set; }
        public int QCThisMonthy { get; set; }
        public int QCThisYear { get; set; }

        public int PrintedToday { get; set; }
        public int PrintedThisWeek { get; set; }
        public int PrintedThisMonthy { get; set; }
        public int PrintedThisYear { get; set; }
    }
}