﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NursingDashboard.Models
{
    public class NursingOperationSummary
    {
        public int NotCaptured { get; set; }

        public int NotQCd { get; set; }

        public int NotPrinted { get; set; }

        public TimeSpan BatchToCapture { get; set; }

        public TimeSpan CaptureToQC { get; set; }

        public TimeSpan QCToPrint { get; set; }
    }
}