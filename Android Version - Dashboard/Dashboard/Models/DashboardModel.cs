﻿using DotNet.Highcharts;
using Nursing_Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NursingDashboard.Models
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            SliderPanel = new List<ChartItem>();
            MonthList = new Dictionary<string, string>();
            YearList = new Dictionary<string, string>();
        }

        public string PopupMsg { get; set; }

        public List<ChartItem> SliderPanel { get; set; }

        public ChartItem WeeklyData { get; set; }

        public Dictionary<string, string> MonthList { get; set; }

        public Dictionary<string, string> YearList { get; set; }


        #region Nursing

        public NursingSummaryInfo NursingSummary { get; set; }

        public NursingOperationSummary NursingOperationSummary { get; set; }

        public List<NursingReport> NursingLastRegistered { get; set; }

        public List<NursingReport> NursingNextDeliveries { get; set; }

        public List<NursingReport> LastTenCardsPrinted { get; set; }

        public Highcharts NursingYearCardsColumnChart { get; set; }

        public Highcharts NursingMonthCardsColumnChart { get; set; }

        public Highcharts NursingOperationAverages { get; set; }

        #endregion
    }
}