﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NursingDashboard.Startup))]
namespace NursingDashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
