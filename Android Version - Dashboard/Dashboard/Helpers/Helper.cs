﻿using Nursing_Common;
using NursingDashboard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace NursingDashboard.Helpers
{
    public static class Helper
    {
        public static string nursingxml = ConfigurationManager.AppSettings["nursingxml"].ToString();

        public static string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        private static int seed = Environment.TickCount;

        private static ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref seed)));

        public static string GenerateRandomColour()
        {
            return String.Format("#{0:X6}", randomWrapper.Value.Next(0x1000000));
        }

        public static string CapitalizeAfter(this string s, IEnumerable<char> chars)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            var charsHash = new HashSet<char>(chars);
            StringBuilder sb = new StringBuilder(s.ToLower());

            // Capitalize first letter
            sb[0] = char.ToUpper(sb[0]);

            for (int i = 0; i < sb.Length - 1; i++)
            {
                if (charsHash.Contains(sb[i]))
                    sb[i + 1] = char.ToUpper(sb[i + 1]);
            }
            return sb.ToString();
        }

        public static string ToFirstLetterUpperSentence(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string[] words = input.ToLower().Split(' ');
                string result = string.Empty;

                foreach (string word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        char[] a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }
       
        public static string ToFirstLetterUpper(string word)
        {
            if (word != string.Empty && word != " ")
            {
                char[] a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }

        public static Dictionary<string, int> GetDictionaryFromList(List<NursingReport> reports, int type, DateTime start, DateTime end)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            string date = string.Empty;

            // Set defaults
            if (type == 1)
            {
                var months = MonthsBetween(start, end);
                foreach (Tuple<string> s in months)
                {
                    result.Add(s.Item1, 0);
                }
            }

            foreach (NursingReport rep in reports)
            {
                if (type == 1)
                    date = rep.PrintDate.ToString("MMM yy");
                else
                    date = rep.PrintDate.ToString("dd");

                if (result.ContainsKey(date))
                {
                    result[date]++;
                }
                else
                    result.Add(date, 1);
            }

            return result;
        }

        public static Dictionary<string, int> GetTotalFromDictionary(Dictionary<string, int> dic)
        {
            int counter = 0;
            Dictionary<string, int> result = new Dictionary<string, int>(dic);

            var keys = new List<string>(result.Keys);

            foreach (string key in keys)
            {
                counter += result[key];
                result[key] = counter;
            }
            return result;
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static IEnumerable<Tuple<string>> MonthsBetween(DateTime startDate, DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;

            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }

            var dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            while (iterator <= limit)
            {
                yield return Tuple.Create(iterator.ToString("MMM yy"));
                iterator = iterator.AddMonths(1);
            }
        }

        private static Dictionary<string, int> GetDictionaryFromXML(string dic)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            string key = string.Empty;
            string value = string.Empty;
            bool capturingkey = false;
            bool capturingvalue = false;

            foreach (char s in dic)
            {
                if (s == ']')
                {
                    if (capturingvalue)
                    {
                        result[key] = int.Parse(value);
                        capturingvalue = false;
                        key = string.Empty;
                        value = string.Empty;
                    }
                }

                if (capturingvalue)
                    value += s;

                if (s == ',')
                {
                    result.Add(key, 0);
                    capturingkey = false;
                    capturingvalue = true;
                }

                if (capturingkey)
                    key += s;

                if (s == '[')
                {
                    capturingkey = true;
                }
            }

            return result;
        }       

        #region XML Requests

        public static Dictionary<string, int> GetTotalChart()
        {
            XDocument doc = XDocument.Load(nursingxml);
            string dic = doc.Root.Element("TotalChartData").Value;
            return GetDictionaryFromXML(dic);
        }

        public static Dictionary<string, int> GetTotalMonthlyChart()
        {
            XDocument doc = XDocument.Load(nursingxml);
            string dic = doc.Root.Element("TotalMonthyChartData").Value;
            return GetDictionaryFromXML(dic);
        }

        public static Dictionary<string, int> GetMonthChartData()
        {
            XDocument doc = XDocument.Load(nursingxml);
            string dic = doc.Root.Element("MonthChartData").Value;
            return GetDictionaryFromXML(dic);
        }

        public static Dictionary<string, Dictionary<string, int>> GetThisYearData()
        {
            XDocument doc = XDocument.Load(nursingxml);

            Dictionary<string, int> captured = GetDictionaryFromXML(doc.Root.Element("MonthChartCapturedData").Value);
            Dictionary<string, int> qcd = GetDictionaryFromXML(doc.Root.Element("MonthChartQCdData").Value);
            Dictionary<string, int> printed = GetDictionaryFromXML(doc.Root.Element("MonthChartPrintedData").Value);

            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();
            result.Add("captured", captured);
            result.Add("qcd", qcd);
            result.Add("printed", printed);

            return result;
        }

        public static Dictionary<string, Dictionary<string, int>> GetOperationAverages()
        {
            Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();
            XDocument doc = XDocument.Load(nursingxml);
            string qcavg = doc.Root.Element("Averages").Element("qcavg").Value;
            string pavg = doc.Root.Element("Averages").Element("printavg").Value;
            string cavg = doc.Root.Element("Averages").Element("captureavg").Value;
            string tavg = doc.Root.Element("Averages").Element("totalavg").Value;

            result.Add("qcavg", GetDictionaryFromXML(qcavg));
            result.Add("printavg", GetDictionaryFromXML(pavg));
            result.Add("captureavg", GetDictionaryFromXML(cavg));
            result.Add("totalavg", GetDictionaryFromXML(tavg));
            return result;
        }

        public static NursingSummaryInfo GetSummaryInfo(NursingSummaryInfo summary)
        {
            XDocument doc = XDocument.Load(nursingxml);
            string dic = doc.Root.Element("SummaryData").Value;
            Dictionary<string, int> sum = GetDictionaryFromXML(dic);

            summary.CapturedToday = sum["CapturedToday"];
            summary.CapturedThisWeek = sum["CapturedThisWeek"];
            summary.CapturedThisMonth = sum["CapturedThisMonth"];
            summary.CapturedThisYear = sum["CapturedThisYear"];

            summary.ReceivedToday = sum["ReceivedToday"];
            summary.ReceivedThisWeek = sum["ReceivedThisWeek"];
            summary.ReceivedThisMonth = sum["ReceivedThisMonth"];
            summary.ReceivedThisYear = sum["ReceivedThisYear"];

            summary.QCToday = sum["QCToday"];
            summary.QCThisWeek = sum["QCThisWeek"];
            summary.QCThisMonth = sum["QCThisMonth"];
            summary.QCThisYear = sum["QCThisYear"];

            summary.PrintedToday = sum["PrintedToday"];
            summary.PrintedThisWeek = sum["PrintedThisWeek"];
            summary.PrintedThisMonth = sum["PrintedThisMonth"];
            summary.PrintedThisYear = sum["PrintedThisYear"];

            return summary;
        }

        public static List<NursingReport> GetLastTenRegistered()
        {
            List<NursingReport> result = new List<NursingReport>();
            XDocument doc = XDocument.Load(nursingxml);

            foreach (XElement xe in doc.Descendants("Record"))
            {
                result.Add(Deserialize<NursingReport>(xe.Value));
            }

            // Ensure we are looking at today's data otherwise show empty
            for (int i = 0; i < result.Count; i++)
            {
                if (result[i].PrintDate != DateTime.Today)
                {
                    result.RemoveAt(i);
                }
            }

            return result;
        }

        public static Dictionary<string, int> GetOperationStats()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            XDocument doc = XDocument.Load(nursingxml);
            string dic = doc.Root.Element("OperationStats").Value;
            return GetDictionaryFromXML(dic);
        }

        public static T Deserialize<T>(string xmlText)
        {
            if (String.IsNullOrWhiteSpace(xmlText)) return default(T);

            using (StringReader stringReader = new System.IO.StringReader(xmlText))
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }
        #endregion
    }

    public static class ListExtenstions
    {
        public static void AddMany<T>(this List<T> list, params T[] elements)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                list.Add(elements[i]);
            }
        }

        public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    // handle duplicate key issue here
                }
            }
        }

        
    }
}