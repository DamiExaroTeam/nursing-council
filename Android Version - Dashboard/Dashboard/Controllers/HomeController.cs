﻿using NursingDashboard.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nursing_Proxy;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Options;
using System.Globalization;
using Nursing_Common;
using NursingDashboard.Helpers;
using DotNet.Highcharts.Helpers;
using System.Configuration;
using Core;
using Excel;

namespace NursingDashboard.Controllers
{
    public class HomeController : Controller
    {
        #region Variables

        public static DateTime LastUpdate;
        public static DateTime LastUpdateAge;
        public static List<NursingReport> LastTenCards;
        public static List<NursingReport> ThisMonthData;
        public static List<NursingReport> LastYearData = new List<NursingReport>();

        List<int> days = new List<int>();
        Dictionary<string, int> captured = new Dictionary<string, int>();
        Dictionary<string, int> qcd = new Dictionary<string, int>();
        Dictionary<string, int> printed = new Dictionary<string, int>();

        public static Dictionary<string, int> chartdatamonth;
        private ChartItem tempyearchart;
        private ChartItem tempavgchart;
        private ChartItem tempmonthchart;
        private Dictionary<string, Color> ColourDictionary;
        private bool SaveTemp;
        private bool useXml = bool.Parse(ConfigurationManager.AppSettings["useXml"].ToString());

        Color Others = ColorTranslator.FromHtml("#467467");
        List<Color> Colours = new List<Color>() {
            { ColorTranslator.FromHtml("#2f7ed8") },
            { ColorTranslator.FromHtml("#FF7C30") },
            { ColorTranslator.FromHtml("#8bbc21") },
            { ColorTranslator.FromHtml("#910000") },
            { ColorTranslator.FromHtml("#1aadce") },
            { ColorTranslator.FromHtml("#492970") },
            { ColorTranslator.FromHtml("#f28f43") },
            { ColorTranslator.FromHtml("#1E55CC") },
            { ColorTranslator.FromHtml("#c42525") },
            { ColorTranslator.FromHtml("#77a1e5") },
            { ColorTranslator.FromHtml("#99FF99") },
            { ColorTranslator.FromHtml("#666666") }
        };

        #endregion

        #region Actions

        public ActionResult Index()
        {
            try
            {
                ViewBag.Header = "Nursing & Midwives Council";
                DashboardModel model = new DashboardModel();
                NursingProxy proxy = new NursingProxy();

                // Keep data so we can also get for big chart without having to query for the same thing twice
                SaveTemp = true;

                DateTime starttime = DateTime.Today.AddYears(-1);

                model.NursingYearCardsColumnChart = GetAllNursingRegisteredYear(1, false);
                model.NursingMonthCardsColumnChart = GetAllNursingRegisteredMonth(1, false);
                model.NursingSummary = GetsummaryData();

                if (!useXml)
                    model.NursingLastRegistered = proxy.NursingChannel.GetLastTenRegistered();
                else
                    model.NursingLastRegistered = Helpers.Helper.GetLastTenRegistered();

                model.NursingOperationAverages = GetAveragesYear(1, false);
                model.NursingOperationSummary = GetOperationsSummaryData();
                LastTenCards = model.NursingLastRegistered;

                // Slider Panel
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigRegistrations", Chart = GetTotalChartData(LastYearData, tempyearchart, true), ChartType = "_ChartTemplate" });
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigDaily", Chart = GetMonthChart(ThisMonthData, tempmonthchart), ChartType = "_ChartTemplate" });
                model.SliderPanel.Add(new ChartItem() { ChartName = "BigOprAvg", Chart = GetAverageChartData(tempavgchart, true), ChartType = "_ChartTemplate" });

                // Show play buttons
                ViewBag.ShowPlayButtons = 1;

                // Data for the slider
                string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;
                model.MonthList.Add("0", "Full Year");
                for (int i = 0; i < names.Length; i++)
                {
                    if (!string.IsNullOrEmpty(names[i]))
                        model.MonthList.Add((i + 1).ToString(), names[i]);
                }
                model.MonthList.Add("100", "Till Date");

                // years
                for (int i = 2016; i < DateTime.Now.Year + 1; i++)
                {
                    model.YearList.Add(i.ToString(), i.ToString());
                }

                // Show refresh
                ViewBag.ShowRefresh = 1;

                // How often should we refresh 
                if (useXml)
                    ViewBag.RefreshTime = (1000 * 60 * 15); // 15mins
                else
                    ViewBag.RefreshTime = (1000 * 60 * 5); // 5mins

                return View(model);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Json

        public ActionResult SearchRegistrationRange(string selectyearval, string selectmonthval)
        {
            DashboardModel model = new DashboardModel();
            ChartItem item = new ChartItem();

            if (string.IsNullOrEmpty(selectyearval))
                selectyearval = DateTime.Today.Year.ToString();

            if (string.IsNullOrEmpty(selectmonthval))
                selectmonthval = "0";

            int year = int.Parse(selectyearval);
            int month = int.Parse(selectmonthval);

            if (!string.IsNullOrEmpty(selectmonthval))
            {
                // Get whole year
                if (month == 0)
                {
                    item.Chart = GetAllNursingRegisteredYear(1, true);
                }
                else
                {
                    // Get till today
                    if (month == 100)
                    {
                        item.Chart = GetAllNursingRegisteredYear(1, true);
                    }
                    else
                        // For a specific month
                        item.Chart = GetAllNursingRegisteredMonth(1, new DateTime(year, month, 1), true);

                }
            }

            return Json(new { success = 1, page = Helpers.Helper.JsonPartialView(this, "_ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSliderData()
        {
            try
            {
                int newday = 0;
                NursingProxy proxy = new NursingProxy();
                DashboardModel model = new DashboardModel();
            
                // Get charts
                if (newday == 1)
                {
                    model.NursingYearCardsColumnChart = GetAllNursingRegisteredYear(1, false);
                   
                    // Slider Panel
                    model.SliderPanel.Add(new ChartItem() { ChartName = "BigRegistrations", Chart = GetChartData(Helpers.Helper.GetDictionaryFromList(LastYearData, 1, DateTime.Today.AddYears(-1), DateTime.Today), tempyearchart, true), ChartType = "_ChartTemplate" });
                    model.SliderPanel.Add(new ChartItem() { ChartName = "BigRegistrationsMonth", Chart = GetChartData(Helpers.Helper.GetDictionaryFromList(ThisMonthData, 2, DateTime.Today, DateTime.Today), tempmonthchart, true), ChartType = "_ChartTemplate" });

                    return Json(new
                    {
                        success = 1,
                        NursingRegistrationsColumnChart = Helpers.Helper.JsonPartialView(this, "_ChartTemplate", model.NursingYearCardsColumnChart),
                        NursingMonthRegistrationsColumnChart = Helpers.Helper.JsonPartialView(this, "_ChartTemplate", model.NursingMonthCardsColumnChart),
                        page = Helpers.Helper.JsonPartialView(this, "_SliderPanel", model),
                        newday = 1
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.NursingSummary = GetsummaryData();
                    if (!useXml)
                        model.NursingLastRegistered = proxy.NursingChannel.GetLastTenRegistered();
                    else
                        model.NursingLastRegistered = Helpers.Helper.GetLastTenRegistered();
                    LastTenCards = model.NursingLastRegistered;

                    return Json(new
                    {
                        success = 1,
                        NursingSummary = Helpers.Helper.JsonPartialView(this, "_SummaryTemplate", model.NursingSummary),
                        NursingLastRegistered = Helpers.Helper.JsonPartialView(this, "_MiniPassportReport", model.NursingLastRegistered),
                        newday = 0
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetMemberDetails(int cardid, string surname)
        {
            try
            {
                Nursing_Common.NursingReport card = LastTenCards.Where(x => x.CardId == cardid && x.Surname == surname).FirstOrDefault();

                if (card != null)
                {
                    return Json(new { status = 1, page = Helpers.Helper.JsonPartialView(this, "_ViewMember", card) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Charts

        private NursingSummaryInfo GetsummaryData()
        {
            NursingSummaryInfo summary = new NursingSummaryInfo();

            if (!useXml)
            {
                DateTime today = DateTime.Today;

                List<NursingReport> year = new NursingProxy().NursingChannel.GetThisYear();
                Dictionary<DateTime, int> captureddic = new NursingProxy().NursingChannel.GetCapturedThisYear();

                summary.CapturedToday = captureddic.Where(x => x.Key.Date == today).Sum(x => x.Value);
                summary.CapturedThisWeek = captureddic.Where(x => x.Key.Date >= today.StartOfWeek(DayOfWeek.Monday) && x.Key.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Sum(x => x.Value);
                summary.CapturedThisMonth = captureddic.Where(x => x.Key.Month == today.Month).Sum(x => x.Value);
                summary.CapturedThisYear = captureddic.Where(x => x.Key.Year == today.Year).Sum(x => x.Value);

                summary.ReceivedToday = year.Where(x => x.DateReceived.Date == today).Count();
                summary.ReceivedThisWeek = year.Where(x => x.DateReceived.Date >= today.StartOfWeek(DayOfWeek.Monday) && x.DateReceived.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count();
                summary.ReceivedThisMonth = year.Where(x => x.DateReceived.Month == today.Month).Count();
                summary.ReceivedThisYear = year.Where(x => x.DateReceived.Year == today.Year).Count();

                summary.QCToday = year.Where(x => x.DateQCd.Date == today).Count();
                summary.QCThisWeek = year.Where(x => x.DateQCd.Date >= today.StartOfWeek(DayOfWeek.Monday) && x.DateQCd.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count();
                summary.QCThisMonth = year.Where(x => x.DateQCd.Month == today.Month).Count();
                summary.QCThisYear = year.Where(x => x.DateQCd.Year == today.Year).Count();

                summary.PrintedToday = year.Where(x => x.PrintDate.Date == today).Count();
                summary.PrintedThisWeek = year.Where(x => x.PrintDate.Date >= today.StartOfWeek(DayOfWeek.Monday) && x.PrintDate.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count();
                summary.PrintedThisMonth = year.Where(x => x.PrintDate.Month == today.Month).Count();
                summary.PrintedThisYear = year.Where(x => x.PrintDate.Year == today.Year).Count();
            }
            else
            {
                summary = Helpers.Helper.GetSummaryInfo(summary);
            }

            return summary;
        }

        private NursingOperationSummary GetOperationsSummaryData()
        {
            NursingOperationSummary summary = new NursingOperationSummary();
            Dictionary<string, int> stats = new Dictionary<string, int>();

            if (!useXml)
            {
                stats = new NursingProxy().NursingChannel.GetOperationsStats();
            }
            else
            {
                stats = Helpers.Helper.GetOperationStats();
            }

            summary.NotPrinted = stats["print"];
            summary.NotQCd = stats["qc"];
            summary.NotCaptured = stats["capture"];

            return summary;
        }

        private Highcharts GetAllNursingRegisteredMonth(int ChartType, DateTime month, bool isBig)
        {
            // Get data
            Dictionary<string, int> regvscount = new NursingProxy().NursingChannel.GetNursingRegistrationsData(month, DateTime.MinValue, 1);

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigPshipRegistrations" : "PshipRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                HideExportButtons = !isBig,
                TitleStyle = "",
                Title = "Card Prints",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Month",
                yAxisTitle = "No. of cards",
                LegendLayout = Layouts.Vertical,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = -10,
                LegendY = 100,
                HideLegend = !isBig,
                LegendPadding = 20
            };

            // Get chart data
            Highcharts result = GetChartData(regvscount, chart, isBig);

            return result;
        }

        private Highcharts GetAllNursingRegisteredYear(int ChartType, bool isBig)
        {
            List<NursingReport> report = new List<NursingReport>();

            // Get data
            if (!useXml)
                report = new NursingProxy().NursingChannel.GetLastYear();

            // Define chart for big one
            tempyearchart = new ChartItem()
            {
                ChartName = "BigNursingRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = false,
                Title = "Cards Printed",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Month",
                yAxisTitle = "Cards Printed",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = 0,
                LegendY = 100,
                HideLegend = false,
                LegendPadding = 20
            };

            if (SaveTemp && !useXml)
            {
                LastYearData = report;
            }

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigNursingRegistrations" : "NursingRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Card Prints" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Month",
                yAxisTitle = "Cards Printed",
                LegendLayout = isBig ? Layouts.Horizontal : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            Highcharts result = GetTotalChartData(LastYearData, chart, isBig);

            return result;
        }

        private Highcharts GetAveragesYear(int ChartType, bool isBig)
        {
            // Define chart for big one
            tempavgchart = new ChartItem()
            {
                ChartName = "BigNursingOperationAvg",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = false,
                Title = "Operation Averages",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Month",
                yAxisTitle = "Hours",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = 0,
                LegendY = 100,
                HideLegend = false,
                LegendPadding = 20
            };

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigNursingOperationAvg" : "NursingOperationAvg",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Operation Averages" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Month",
                yAxisTitle = "Hours",
                LegendLayout = isBig ? Layouts.Horizontal : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            Highcharts result = GetAverageChartData(chart, isBig);

            return result;
        }

        private Highcharts GetAllNursingRegisteredMonth(int ChartType, bool isBig)
        {
            List<NursingReport> report = new List<NursingReport>();
            chartdatamonth = new Dictionary<string, int>();

            // Define chart for big one
            tempmonthchart = new ChartItem()
            {
                ChartName = "BigNursingMonthRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = false,
                Title = "Cards Printed",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Day",
                yAxisTitle = "No. of cards",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = 0,
                LegendY = 100,
                HideLegend = false,
                LegendPadding = 20
            };

            // Define chart
            ChartItem chart = new ChartItem()
            {
                ChartName = isBig ? "BigNursingMonthRegistrations" : "NursingMonthRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Card Prints" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                xAxisText = "Day",
                yAxisTitle = "No. of cards",
                LegendLayout = isBig ? Layouts.Horizontal : Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5,
            };

            // Get days in this month
            int year = DateTime.Today.Year;
            int month = DateTime.Today.Month;
            int day = DateTime.Today.Day;
            days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).Select(d => d).ToList();

            // Set dictionary default values
            foreach (int i in days)
            {
                if (i <= DateTime.Today.Day)
                {
                    captured.Add(i.ToString(), 0);
                    qcd.Add(i.ToString(), 0);
                    printed.Add(i.ToString(), 0);
                }
            }

            // Get data
            if (!useXml)
            {
                report = new NursingProxy().NursingChannel.GetThisYear();
                
                // Get chart data for each
                foreach (NursingReport rep in report)
                {
                    if (rep.DateCaptured.Year == year && rep.DateCaptured.Month == month)
                        captured[rep.DateCaptured.Day.ToString()]++;

                    if (rep.DateQCd.Year == year && rep.DateQCd.Month == month)
                        qcd[rep.DateQCd.Day.ToString()]++;

                    if (rep.PrintDate.Year == year && rep.PrintDate.Month == month)
                        printed[rep.PrintDate.Day.ToString()]++;
                }                
            }
            else
            {
                Dictionary<string, Dictionary<string, int>> dics = Helpers.Helper.GetThisYearData();

                foreach (KeyValuePair<string, int> pair in captured)
                    captured[pair.Key] = dics["captured"][pair.Key];

                foreach (KeyValuePair<string, int> pair in qcd)
                    qcd[pair.Key] = dics["qcd"][pair.Key];

                foreach (KeyValuePair<string, int> pair in printed)
                    printed[pair.Key] = dics["printed"][pair.Key];
            }

            if (SaveTemp)
            {
                ThisMonthData = report;
            }

            return GetMonthChart(report, chart);
        }

        #endregion

        #region Operations

        private Highcharts GetMonthChart(List<NursingReport> report, ChartItem chart)
        {
            // Get series
            int z = 0;
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (report.Count == 0 && !useXml)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                List<int> printseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in printed)
                {
                    printseries.Add(innerval.Value);
                }

                ser.Add("Printed", printseries);

                List<int> capturedseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in captured)
                {
                    capturedseries.Add(innerval.Value);
                }

                ser.Add("Captured", capturedseries);

                List<int> qcseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in qcd)
                {
                    qcseries.Add(innerval.Value);
                }

                ser.Add("QCd", qcseries);

                // Assign series
                series = new Series[ser.Keys.Count];
                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = Colours[z], Visible = true, Marker = new PlotOptionsSeriesMarker() { Enabled = true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Set x-axis data
            string[] xAxis = days.Select(x => x.ToString()).ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetTotalChartData(List<NursingReport> LastYearData, ChartItem chart, bool isBig)
        {
            Dictionary<string, int> regvscount = new Dictionary<string, int>();
            int z = 0;

            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (LastYearData.Count == 0 && !useXml)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;
                Dictionary<string, int> monthly = new Dictionary<string, int>();

                if (useXml)
                {
                    monthly = Helpers.Helper.GetTotalMonthlyChart();
                    regvscount = Helpers.Helper.GetTotalChart();
                }
                else
                {
                    monthly = Helpers.Helper.GetDictionaryFromList(LastYearData, 1, DateTime.Today.AddYears(-1), DateTime.Today);

                    // Get total
                    regvscount = Helpers.Helper.GetTotalFromDictionary(monthly);
                }

                List<int> enrolseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in regvscount)
                {
                    enrolseries.Add(innerval.Value);
                }

                ser.Add("Registered", enrolseries);

                // monthly
                List<int> monthlyseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in monthly)
                {
                    monthlyseries.Add(innerval.Value);
                }

                ser.Add("Monthly", monthlyseries);

                // Assign series
                series = new Series[ser.Keys.Count];

                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series
                    {
                        PlotOptionsSeries = new PlotOptionsSeries()
                        {
                            LineWidth = 3,
                            Color = Colours[z],
                            Visible = true,
                            Marker = new PlotOptionsSeriesMarker()
                            {
                                Enabled = true
                            }
                        },
                        Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray()),
                        Type = val.Key == "Monthly" ? ChartTypes.Column : ChartTypes.Line
                    };

                    z++;
                }
            }

            // Set x-axis data
            int arraylength = regvscount.Keys.Count;
            string[] xAxis = regvscount.Keys.ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetAverageChartData(ChartItem chart, bool isBig)
        {
            int z = 0;

            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Dictionary<string, int> capture = new Dictionary<string, int>();
            Dictionary<string, int> qc = new Dictionary<string, int>();
            Dictionary<string, int> print = new Dictionary<string, int>();
            Dictionary<string, int> total = new Dictionary<string, int>();

            Series[] series;
            DateTime start = DateTime.Today.AddYears(-1);
            DateTime end = DateTime.Today;

            if (LastYearData.Count == 0 && !useXml)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;
                Dictionary<string, Dictionary<string, int>> avgs = new Dictionary<string, Dictionary<string, int>>();                

                if (useXml)
                {
                    avgs = Helpers.Helper.GetOperationAverages();
                }
                else
                    avgs = new NursingProxy().NursingChannel.GetOperationAverages(start, end);

                qc = avgs["qcavg"];
                print = avgs["printavg"];
                capture = avgs["captureavg"];
                total = avgs["totalavg"];

                List<int> captureseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in capture)
                {
                    captureseries.Add(innerval.Value);
                }

                List<int> qcseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in qc)
                {
                    qcseries.Add(innerval.Value);
                }

                List<int> printseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in print)
                {
                    printseries.Add(innerval.Value);
                }

                List<int> totalseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in total)
                {
                    totalseries.Add(innerval.Value);
                }

                ser.Add("QC Avg", qcseries);
                ser.Add("Capture Avg", captureseries);
                ser.Add("Print Avg", printseries);
                ser.Add("Total Avg", totalseries);

                // Assign series
                series = new Series[ser.Keys.Count];

                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series
                    {
                        PlotOptionsSeries = new PlotOptionsSeries()
                        {
                            LineWidth = 3,
                            Color = Colours[z],
                            Visible = true,
                            Marker = new PlotOptionsSeriesMarker()
                            {
                                Enabled = true
                            }
                        },
                        Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray()),
                        Type = ChartTypes.Line
                    };

                    z++;
                }
            }

            // Set x-axis data
            var monthlist = new List<string>();
            string format = "MMM yy";

            for (var d = start; d <= end; d = d.AddMonths(1))
            {
                monthlist.Add(d.ToString(format));
            }

            string[] xAxis = monthlist.ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetChartData(Dictionary<string, int> regvscount, ChartItem chart, bool isBig)
        {
            int z = 0;

            // Get series
            Dictionary<string, List<int>> ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (regvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                List<int> enrolseries = new List<int>();
                foreach (KeyValuePair<string, int> innerval in regvscount)
                {
                    enrolseries.Add(innerval.Value);
                }

                ser.Add("Registered", enrolseries);

                // Assign series
                series = new Series[ser.Keys.Count];
                foreach (KeyValuePair<string, List<int>> val in ser)
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = Colours[z], Visible = true, Marker = new PlotOptionsSeriesMarker() { Enabled = true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Set x-axis data
            string[] xAxis = regvscount.Keys.ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts CreateChart(string[] timespan, Series[] series, ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = item.ChartSeriesType,
                    MarginRight = item.MarginRight,
                    ClassName = item.ClassName
                })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetSubtitle(new Subtitle { Style = item.SubtitleStyle, Text = item.Subtitle })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Max = timespan.Length - 1, StartOnTick = true, Title = new XAxisTitle { Text = item.xAxisText } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = item.yAxisTitle }
                })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetLegend(new Legend
                {
                    Layout = item.LegendLayout,
                    Align = item.LegendAlign,
                    VerticalAlign = item.LegendVerticalAlign,
                    X = item.LegendX,
                    Y = item.LegendY,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !item.HideLegend,
                    Padding = item.LegendPadding,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } }
                })
                .SetSeries(series);

            return chart;
        }

        private Highcharts CreatePieChart(ChartItem item)
        {
            Highcharts chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" })
                .SetExporting(new Exporting()
                {
                    Enabled = !item.HideExportButtons
                })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Distance = item.LabelDistance,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true)
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Verifications",
                    Data = new Data(item.PieChartData.ToArray())
                });

            return chart;
        }

        private int MathMod(int a, int b)
        {
            return ((a % b) + b) % b;
        }

        private void ReadExcelFile()
        {
            // read excel file
            List<worksheet> worksheets = Workbook.Worksheets(@"C:\\Temp\BatchAvg.xlsx").ToList();
            var calc = new Calculation(new List<DateTime>(), new OpenHours("09:00;16:00"));
            List<double> captureavg = new List<double>();
            List<double> qcavg = new List<double>();
            List<double> printavg = new List<double>();

            for (int i = 0; i < worksheets[0].Rows.Count(); i++)
            {
                Row row = worksheets[0].Rows[i];

                if (i > 0)
                {
                    if (row.Cells[0] == null)
                        break;

                    DateTime registrationdate = DateTime.FromOADate(Convert.ToDouble(row.Cells[1].Value));
                    DateTime capturedate = DateTime.FromOADate(Convert.ToDouble(row.Cells[3].Value));
                    double minutes = calc.getElapsedMinutes(registrationdate, capturedate);
                    double hours = minutes / 60;

                    captureavg.Add(hours);
                }
                else
                    continue;
            }

            for (int i = 0; i < worksheets[1].Rows.Count(); i++)
            {
                Row row = worksheets[1].Rows[i];

                if (i > 0)
                {
                    if (row.Cells[1] == null)
                        break;

                    DateTime capturedate = DateTime.FromOADate(Convert.ToDouble(row.Cells[1].Value));
                    DateTime qcdate = DateTime.FromOADate(Convert.ToDouble(row.Cells[2].Value));
                    double minutes = calc.getElapsedMinutes(capturedate, qcdate);
                    double hours = minutes / 60;

                    qcavg.Add(hours);
                }
                else
                    continue;
            }

            for (int i = 0; i < worksheets[2].Rows.Count(); i++)
            {
                Row row = worksheets[2].Rows[i];

                if (i > 1)
                {
                    if (row.Cells[0] == null)
                        break;

                    DateTime qcdate = DateTime.FromOADate(Convert.ToDouble(row.Cells[3].Value));
                    DateTime printdate = DateTime.FromOADate(Convert.ToDouble(row.Cells[4].Value));
                    double minutes = calc.getElapsedMinutes(qcdate, printdate);
                    double hours = minutes / 60;

                    printavg.Add(hours);
                }
                else
                    continue;
            }

            double avgcapture = captureavg.Average();
            double avgqc = qcavg.Average();
            double avgprint = printavg.Average();
        }

        #endregion
    }
}