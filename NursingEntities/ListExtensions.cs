﻿using System;
using System.Collections.Generic;

namespace Nursing_Common
{
    public static class ListExtenstions
    {
        public static void AddMany<T>(this List<T> list, params T[] elements)
        {
            for (int index = 0; index < elements.Length; ++index)
                list.Add(elements[index]);
        }

        public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
        {
            if (collection == null)
                throw new ArgumentNullException("Collection is null");
            foreach (KeyValuePair<T, S> keyValuePair in collection)
            {
                if (!source.ContainsKey(keyValuePair.Key))
                    source.Add(keyValuePair.Key, keyValuePair.Value);
            }
        }
    }
}
