﻿using System;
using System.Runtime.Serialization;

namespace Nursing_Common
{
    public class NursingReport
    {
        [DataMember]
        public int Total { get; set; }

        [DataMember]
        public int TotalThisWeek { get; set; }

        [DataMember]
        public int TotalThisMonth { get; set; }

        [DataMember]
        public int Today { get; set; }       

        [DataMember]
        public int CardId { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string Middename { get; set; }

        [DataMember]
        public string Pri { get; set; }

        [DataMember]
        public DateTime Dob { get; set; }

        [DataMember]
        public byte[] Photo { get; set; }

        [DataMember]
        public DateTime DateReceived { get; set; }

        [DataMember]
        public DateTime DateCaptured { get; set; }

        [DataMember]
        public DateTime DateQCd { get; set; }

        [DataMember]
        public DateTime PrintDate { get; set; }
    }
}
