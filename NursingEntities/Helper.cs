﻿using System;
using System.Collections.Generic;

namespace Nursing_Common
{
    public static class Helper
    {
        public static string ToFirstLetterUpperSentence(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;
            string[] strArray = input.ToLower().Split(' ');
            string str1 = string.Empty;
            foreach (string str2 in strArray)
            {
                if (str2 != string.Empty && str2 != " ")
                {
                    char[] charArray = str2.ToCharArray();
                    charArray[0] = char.ToUpper(charArray[0]);
                    str1 = str1 != string.Empty ? str1 + " " + new string(charArray) : str1 + new string(charArray);
                }
            }
            return str1;
        }

        public static string ToFirstLetterUpper(string word)
        {
            if (!(word != string.Empty) || !(word != " "))
                return string.Empty;
            char[] charArray = word.ToLower().ToCharArray();
            charArray[0] = char.ToUpper(charArray[0]);
            return new string(charArray);
        }

        public static IEnumerable<Tuple<string>> MonthsBetween(DateTime startDate, DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;
            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }
            for (; iterator <= limit; iterator = iterator.AddMonths(1))
                yield return Tuple.Create(iterator.ToString("MMM yy"));
        }
    }
}
