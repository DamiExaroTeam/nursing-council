﻿using System.Web.Optimization;

namespace NursingDashboard
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js", "~/Scripts/jquery.validate*", "~/Scripts/jquery.validate.unobtrusive.js", "~/Scripts/General/master.js"));

            bundles.Add(new ScriptBundle("~/bundles/general").Include("~/Scripts/General/general.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashboardbundle").Include("~/Scripts/General/libs/jquery/jquery-ui.js", "~/Scripts/General/highcharts/highcharts.js", "~/Scripts/General/highcharts/modules/exporting.js", "~/Scripts/General/libs/touchpunch/jquery.ui.touch-punch.js", "~/Scripts/General/libs/gitter/jquery.gritter.js", "~/Scripts/General/libs/datatables/jquery.dataTables.js", "~/Scripts/General/jquery.movingboxes.min.js", "~/Scripts/General/jquery.select2.min.js", "~/Scripts/General/jquery-sDashboard.js", "~/Scripts/General/jquery.lightbox_me.js"));

            bundles.Add(new StyleBundle("~/Content/dashboardbundle").Include("~/Content/General/jquery-ui.css", "~/Content/General/sDashboard.css", "~/Content/General/jquery.gritter.css", "~/Content/bootstrap.css", "~/Content/General/datatables.css", "~/Content/General/movingboxes.css", "~/Content/General/select2.css"));

            bundles.Add(new StyleBundle("~/Content/master").Include("~/Content/General/masterpage.css", "~/Content/General/misc.css"));

            bundles.Add(new StyleBundle("~/Content/summarystyles").Include("~/Content/General/summarystyles.css"));
            bundles.Add(new StyleBundle("~/Content/homepage").Include("~/Content/General/homepage.css"));
            bundles.Add(new ScriptBundle("~/bundles/nursing").Include("~/Scripts/nursing.js"));
        }
    }
}
