﻿using System;

namespace NursingDashboard.Models
{
    public class NursingOperationSummary
    {
        public int NotCaptured { get; set; }

        public int NotQCd { get; set; }

        public int NotPrinted { get; set; }

        public TimeSpan BatchToCapture { get; set; }

        public TimeSpan CaptureToQc { get; set; }

        public TimeSpan QcToPrint { get; set; }
    }
}