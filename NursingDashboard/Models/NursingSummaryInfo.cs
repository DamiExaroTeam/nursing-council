﻿namespace NursingDashboard.Models
{
    public class NursingSummaryInfo
    {
        public int ReceivedToday { get; set; }

        public int ReceivedThisWeek { get; set; }

        public int ReceivedThisMonth { get; set; }

        public int ReceivedThisYear { get; set; }

        public int CapturedToday { get; set; }

        public int CapturedThisWeek { get; set; }

        public int CapturedThisMonth { get; set; }

        public int CapturedThisYear { get; set; }

        public int QcToday { get; set; }

        public int QcThisWeek { get; set; }

        public int QcThisMonth { get; set; }

        public int QcThisYear { get; set; }

        public int PrintedToday { get; set; }

        public int PrintedThisWeek { get; set; }

        public int PrintedThisMonth { get; set; }

        public int PrintedThisYear { get; set; }
    }
}