﻿using Microsoft.Owin;
using NursingDashboard;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace NursingDashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
