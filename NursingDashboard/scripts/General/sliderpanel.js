﻿// Variables
var refreshIntervalId;
var refreshcounter = 0;
var waitsmallid = 0;
var stopspinning = 0;
var isspinning = 0;
var waitsmall = false;
var retrievingdata = false;
var beenclicked = false;
var isPaused = true;
var dontPause = false;
var dontPauseMain = false;

// Duplicative functions from each dashboard js
$(function () {
    // Initialize dashboard
    $("#nursingDashboard").sDashboard({
        dashboardData: dashboardJSON
    });

    // Set heights
    var boxheight = ($(window).height() - 95) / 2;
    //$('.sDashboard li').css('height', boxheight + 'px');

    // Events
    $(".refreshpage").click(function () {
        DoRefresh();
    });

    $(document).on('click', '.sDashboard-circle-plus-icon', function () {
        $('.startSlide').removeClass('stopSlide');
    });

    $(document).on('click', '.sDashboardExpand', function () {
        $('.startSlide').removeClass('stopSlide');
        var div = $(this).closest('.sDashboardWidget').find('.multichart');
        $("#multiviewdiv").fadeOut(function () {
            $("#sliderdiv").hide().css('visibility', 'visible').fadeIn();

            // select correct slide
            var slider = $('#slider').data('movingBoxes');
            slider.change(div.attr('data-slidernum'));
        });
    });

    // For tablets, when the orientation changes, refresh window
    window.addEventListener("orientationchange", updateOrientation); 

    function updateOrientation(e) {
        $(window).trigger("resize");
    }

    // At a set interval, refresh dashboard
    var interval = $('#refreshtime').val();
    refreshIntervalId = window.setInterval(function () {
        if ($("#sliderdiv").css('display') == 'none' || $("#sliderdiv").css('visibility') == 'hidden') {
            if (refreshcounter == 5) {
                // On the 5th interval show the slider
                refreshcounter = 1;
                var slider = $('#slider').data('movingBoxes');

                if ($('#slider').length && $('.startSlide').hasClass('stopSlide')) {
                    $("#multiviewdiv").fadeOut(function () {
                        $('.graphinfodiv').show();
                        $("#sliderdiv").fadeIn();

                        // Go to first slide
                        slider.change(1);

                        // Start the slide
                        if (!$('#btnplay').hasClass('pause')) {
                            $('#btnplay').click();
                        }
                    });
                }
            }
            else {
                DoRefresh();
            }

            refreshcounter++;
        }
    }, interval);

    // resize the window to ensure all graphs fit properly
    $(window).trigger("resize");
});


// Functions
function rotate() {
    var $elie = $(".refreshpage"), degree = 0, timer;
    $elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)' });
    $elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)' });
    timer = setTimeout(function () {
        ++degree; rotate();

        if (stopspinning == 1 && (degree % 360 === 0)) {
            stopspinning = 0;
            isspinning = 0;
            clearTimeout(timer);
        }
    }, 5);
}

function ShowPassportPreview(id) {
    $('.startSlide').removeClass('stopSlide');

    if (waitsmall) {
        waitsmallid = id;
    }
    else {
        retrievingdata = true;
        $('.popupinnerdiv').fadeOut(function () {
            $('#ppinfodiv').lightbox_me({
                centered: true,
                onLoad: function () {
                    $('#ppinfodiv').addClass("loading");
                    retrievingdata = true;
                    $.ajax({
                        type: "GET",
                        url: "/Home/GetMemberDetails",
                        cache: false,
                        data: { memberid: id },
                        dataType: "json",
                        error: function (result) {
                            $('#ppinfodiv').removeClass("loading");
                            $('#popupinnerdiv').fadeIn();
                            retrievingdata = false;
                            ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                        },
                        success: function (result) {
                            retrievingdata = false;
                            $('#ppinfodiv').removeClass("loading");
                            if (result.status == 0) {
                                ShowPopup("Incomplete passport data.");
                            }

                            if (result.status == 1) {
                                $('.popupinnerdiv').html(result.page);
                                $('.popupinnerdiv').fadeIn();
                            }

                            if (result.status == 2) {
                                //window.location.href = '/';
                            }
                        }
                    });
                }
            });
        });
    }
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

$(function () {
    // Combo boxes for search
    $("#selectyear").select2({
        placeholder: "Select year",
        minimumResultsForSearch: 8
    }).on("select2-opening", function () {
        // Pause slider
        if (!$('#btnplay').hasClass('play'))
            $('#btnplay').click();
    }).on("change", function () {
        $("#selectyearval").val($(this).val());
    });

    $("#selectmonth").select2({
        placeholder: "Select month",
        minimumResultsForSearch: 8
    })
        .on("change", function () {
            $("#selectmonthval").val($(this).val());
    })
        .on("select2-opening", function () {
        // Pause slider
        if (!$('#btnplay').hasClass('play'))
            $('#btnplay').click();
    });

    $('#slider').movingBoxes({
        startPanel: 1,
        fixedHeight: false,
        hashtags: false,
        reducedSize: 1,
        buildNav: true,
        completed: function () {

        },
        wrap: true,
        buildNav: true,
        navFormatter: function () { return "&#9679;"; }
    });

    $('.mb-right').click(function () {
        $('#slider').data('movingBoxes').goForward();

        if (slider.curPanel == 4 && $('.startSlide').hasClass('stopSlide')) {
            dontPauseMain = true;
            $('#btnplay').click();
            $("#sliderdiv").fadeOut(function () {
                $("#multiviewdiv").fadeIn();
            });
        }
        else {
            if (!dontPause) {
                if ($('#btnplay').hasClass('pause')) {
                    $('#btnplay').addClass('play');
                    $('#btnplay').removeClass('pause');
                    $('.startSlide').removeClass('stopSlide');;
                    isPaused = true;
                }
            }

            dontPause = false;
        }
    });

    $('.mb-left').click(function () {
        $('.startSlide').removeClass('stopSlide');
        $('#slider').data('movingBoxes').goBack();

        //if (!dontPause) {
            if ($('#btnplay').hasClass('pause')) {
                $('#btnplay').addClass('play');
                $('#btnplay').removeClass('pause');
                isPaused = true;
            }
        //}

        //dontPause = false;
    });

    $('.startSlide').click(function () {
        if ($(this).hasClass('stopSlide'))
            $(this).removeClass('stopSlide');
        else
            $(this).addClass('stopSlide');
    });

    $('.gohome').click(function () {
        //if (!dontPauseMain)
        //    $('.startSlide').removeClass('stopSlide');

        if (!$('.startSlide').hasClass('stopSlide'))
            $('.startSlide').addClass('stopSlide');

        $("#sliderdiv").fadeOut(function () {
            $("#multiviewdiv").fadeIn(function () {
                // resize the window to ensure all graphs fit properly
                $(window).trigger("resize");
            });
        });
    });

    $('.hideme').click(function () {
        if ($('.graphinfodiv').hasClass('maketransparent'))
        {
            $('.hideme').text('Hide');
            $('.graphinfodiv').removeClass('maketransparent');
        }
        else
        {
            $('.hideme').text('Show');
            $('.graphinfodiv').addClass('maketransparent');
        }
    });

    $('.play').click(function () {
        if ($(this).hasClass('play')) {
            $(this).addClass('pause');
            $(this).removeClass('play');
            isPaused = false;
        }
        else {
            if (dontPauseMain) {
                dontPauseMain = false;
            }
            else {
                $('.startSlide').removeClass('stopSlide');
            }

            $(this).addClass('play');
            $(this).removeClass('pause');
            isPaused = true;
        }

        dontPause = false;
    });

    // Auto scroll
    window.setInterval(function () {
        var slider = $('#slider').data('movingBoxes');
        if (!isPaused) {
            if (slider.curPanel == slider.totalPanels) {
                $('.gohome').click();
            }
            else {
                dontPause = true;
                slider.goForward();
            }
        }
    }, 7500);
});

