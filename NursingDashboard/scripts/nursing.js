﻿var dashboardJSON;
$(function () {
    // Dashboard data
    dashboardJSON = [
    {
        widgetTitle: "Last 10 Cards printed",
        widgetId: "last10reg",
        widgetContent: $("#NursingLastRegistered")
    },
    {
        widgetTitle: "Total cards printed over last year",
        widgetId: "totalreg",
        widgetContent: $("#NursingRegistrationsColumnChart")
    },
    {
        widgetTitle: "Card status for this month",
        widgetId: "totalmonthreg",
        widgetContent: $("#NursingMonthRegistrationsColumnChart")
    },
    {
        widgetTitle: "Summary information of Cards",
        widgetId: "summaryinfo",
        widgetContent: $("#NursingSummary")
    },
    {
        widgetTitle: "Operational Averages",
        widgetId: "operationavgs",
        widgetContent: $("#NursingAvgChart")
    },
    {
        widgetTitle: "Operational Statistics",
        widgetId: "operationstats",
        widgetContent: $("#NursingOpSummary")
    },
    ];
});

// Functions
function DoRefresh() {
    // Rotate 'refresh' image
    var $elie = $(".refreshpage"), degree = 0, timer;

    if (isspinning == 0) {
        isspinning = 1;
        rotate();
    }

    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Home/GetSliderData",
        cache: false,
        dataType: "json",
        error: function (result) {
            // Stop rotation
            stopspinning = 1;
            retrievingdata = false;
        },
        success: function (result) {
            retrievingdata = false;
            $("#sliderdiv").show().css('visibility', 'hidden');

            // Stop rotation
            stopspinning = 1;

            if (result.newday == "1") {
                $("#sliderdiv").html(result.page);

                // Refresh big charts
                $('#NursingHospitalRegistration').html(result.NursingHospitalRegistration);
                $('#NursingRegistrationsColumnChart').html(result.NursingRegistrationsColumnChart);
                $('#NursingNextDeliveries').html(result.NursingNextDeliveries);
            }

            // Refresh small charts
            $('#NursingSummary').html(result.NursingSummary);
            $('#NursingLastRegistered').html(result.NursingLastRegistered);
            $('#employeessRegisteredDash .sDashboardWidgetHeader').html('<div title="Maximize" class="sDashboard-icon sDashboard-circle-plus-icon "></div>Members Registered Today (' + result.Registrationscount + ')');

            // Refresh last update if its xml
            if (result.xmldate != '')
            {
                $('.titleval').hide().text(result.xmldate).fadeIn();
            }

            // hide it again
            $("#sliderdiv").css('visibility', 'visible').hide();
        }
    });
}

function GetCardDetails(id, surname) {
    $('.startSlide').removeClass('stopSlide');
    retrievingdata = true;

    // remove any other instances created by lightbox
    var duplicateChk = {};

    $('.PopupWindow').each(function () {
        if (duplicateChk.hasOwnProperty(this.id)) {
            $(this).remove();
        } else {
            duplicateChk[this.id] = 'true';
        }
    });

    $('.popupinnerdiv').fadeOut(function () {
        $('#ppinfodiv').lightbox_me({
            centered: true,
            onLoad: function () {
                $('#ppinfodiv').addClass("loading");
                retrievingdata = true;
                $.ajax({
                    type: "GET",
                    url: "/Home/GetMemberDetails",
                    cache: false,
                    data: { cardid: id, surname: surname },
                    dataType: "json",
                    error: function (result) {
                        $('#ppinfodiv').removeClass("loading");
                        $('#popupinnerdiv').fadeIn();
                        retrievingdata = false;
                        ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                    },
                    success: function (result) {
                        retrievingdata = false;
                        $('#ppinfodiv').removeClass("loading");
                        if (result.status == 0) {
                            ShowPopup("Could not find employee");
                        }

                        if (result.status == 1) {
                            $('.popupinnerdiv').html(result.page);
                            $('.popupinnerdiv').fadeIn();

                            // resize the window to ensure all graphs fit properly
                            $(window).trigger("resize");
                        }

                        if (result.status == 2) {
                            //window.location.href = '/';
                        }
                    }
                });
            }
        });
    });
}

