﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using Nursing_Common;
using Nursing_Proxy;
using NursingDashboard.Helpers;
using NursingDashboard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Helper = NursingDashboard.Helpers.Helper;

namespace NursingDashboard.Controllers
{
    public class HomeController : BaseController
    {
        #region Variables

        public static DateTime LastUpdate;
        public static DateTime LastUpdateAge;
        public static List<NursingReport> LastTenCards;
        public static List<NursingReport> ThisMonthData;
        public static List<NursingReport> LastYearData = new List<NursingReport>();

        private List<int> _days = new List<int>();
        private Dictionary<string, int> _captured = new Dictionary<string, int>();
        private Dictionary<string, int> _qcd = new Dictionary<string, int>();
        private Dictionary<string, int> _printed = new Dictionary<string, int>();

        public static Dictionary<string, int> Chartdatamonth;
        private ChartItem _tempyearchart;
        private ChartItem _tempavgchart;
        private ChartItem _tempmonthchart;
        private bool _saveTemp;
        private readonly bool _useXml = bool.Parse(ConfigurationManager.AppSettings["useXml"]);

        private readonly List<Color> _colours = new List<Color>
        {
            ColorTranslator.FromHtml("#2f7ed8"),
            ColorTranslator.FromHtml("#FF7C30"),
            ColorTranslator.FromHtml("#8bbc21"),
            ColorTranslator.FromHtml("#910000"),
            ColorTranslator.FromHtml("#1aadce"),
            ColorTranslator.FromHtml("#492970"),
            ColorTranslator.FromHtml("#f28f43"),
            ColorTranslator.FromHtml("#1E55CC"),
            ColorTranslator.FromHtml("#c42525"),
            ColorTranslator.FromHtml("#77a1e5"),
            ColorTranslator.FromHtml("#99FF99"),
            ColorTranslator.FromHtml("#666666")
        };

        #endregion

        #region Actions

        public ActionResult Index()
        {
            ViewBag.Header = "Nursing & Midwives Council";
            var model = new DashboardModel();
            var proxy = new NursingProxy();

            // Keep data so we can also get for big chart without having to query for the same thing twice
            _saveTemp = true;

            // Get date of XML
            if (_useXml)
                ViewBag.XMLDate = Helper.GetXmlDate();

            model.NursingYearCardsColumnChart = GetAllNursingRegisteredYear(false);
            model.NursingMonthCardsColumnChart = GetAllNursingRegisteredMonth(false);
            model.NursingSummary = GetsummaryData();

            model.NursingLastRegistered = !_useXml ? proxy.NursingChannel.GetLastTenRegistered() : Helper.GetLastTenRegistered();

            model.NursingOperationAverages = GetAveragesYear(false);
            model.NursingOperationSummary = GetOperationsSummaryData();
            LastTenCards = model.NursingLastRegistered;

            // Slider Panel
            model.SliderPanel.Add(new ChartItem
            {
                ChartName = "BigRegistrations",
                Chart = GetTotalChartData(LastYearData, _tempyearchart),
                ChartType = "_ChartTemplate"
            });

            model.SliderPanel.Add(new ChartItem
            {
                ChartName = "BigDaily",
                Chart = GetMonthChart(ThisMonthData, _tempmonthchart),
                ChartType = "_ChartTemplate"
            });

            model.SliderPanel.Add(new ChartItem
            {
                ChartName = "BigOprAvg",
                Chart = GetAverageChartData(_tempavgchart),
                ChartType = "_ChartTemplate"
            });

            // Show play buttons
            ViewBag.ShowPlayButtons = 1;

            // Data for the slider
            var names = DateTimeFormatInfo.CurrentInfo?.MonthNames;
            model.MonthList.Add("0", "Full Year");
            for (var i = 0; i < names?.Length; i++)
            {
                if (!string.IsNullOrEmpty(names[i]))
                    model.MonthList.Add((i + 1).ToString(), names[i]);
            }
            model.MonthList.Add("100", "Till Date");

            // years
            for (var i = 2016; i < DateTime.Now.Year + 1; i++)
            {
                model.YearList.Add(i.ToString(), i.ToString());
            }

            // Show refresh
            ViewBag.ShowRefresh = 1;

            // How often should we refresh 
            if (_useXml)
                ViewBag.RefreshTime = (1000 * 60 * 15); // 15mins
            else
                ViewBag.RefreshTime = (1000 * 60 * 5); // 5mins

            return View(model);
        }

        #endregion

        #region Json

        public ActionResult SearchRegistrationRange(string selectyearval, string selectmonthval)
        {
            var item = new ChartItem();

            if (string.IsNullOrEmpty(selectyearval))
                selectyearval = DateTime.Today.Year.ToString();

            if (string.IsNullOrEmpty(selectmonthval))
                selectmonthval = "0";

            var year = int.Parse(selectyearval);
            var month = int.Parse(selectmonthval);

            if (!string.IsNullOrEmpty(selectmonthval))
            {
                // Get whole year
                if (month == 0)
                {
                    item.Chart = GetAllNursingRegisteredYear(true);
                }
                else
                {
                    // Get till today
                    item.Chart = month == 100 ? GetAllNursingRegisteredYear(true) : GetAllNursingRegisteredMonth(new DateTime(year, month, 1), true);
                }
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "_ChartTemplate", item.Chart)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSliderData()
        {
            var newday = 0;
            var proxy = new NursingProxy();
            var model = new DashboardModel();

            // Get charts
            if (newday == 1)
            {
                model.NursingYearCardsColumnChart = GetAllNursingRegisteredYear(false);

                // Slider Panel
                model.SliderPanel.Add(new ChartItem
                {
                    ChartName = "BigRegistrations",
                    Chart = GetChartData(
                        Helper.GetDictionaryFromList(LastYearData, 1, DateTime.Today.AddYears(-1), DateTime.Today),
                        _tempyearchart),
                    ChartType = "_ChartTemplate"
                });

                model.SliderPanel.Add(new ChartItem
                {
                    ChartName = "BigRegistrationsMonth",
                    Chart = GetChartData(Helper.GetDictionaryFromList(ThisMonthData, 2, DateTime.Today, DateTime.Today),
                        _tempmonthchart),
                    ChartType = "_ChartTemplate"
                });

                return Json(new
                {
                    success = 1,
                    NursingRegistrationsColumnChart =
                    Helper.JsonPartialView(this, "_ChartTemplate", model.NursingYearCardsColumnChart),
                    NursingMonthRegistrationsColumnChart =
                    Helper.JsonPartialView(this, "_ChartTemplate", model.NursingMonthCardsColumnChart),
                    page = Helper.JsonPartialView(this, "_SliderPanel", model),
                    newday = 1
                }, JsonRequestBehavior.AllowGet);
            }

            model.NursingSummary = GetsummaryData();

            model.NursingLastRegistered = !_useXml
                ? proxy.NursingChannel.GetLastTenRegistered()
                : Helper.GetLastTenRegistered();

            LastTenCards = model.NursingLastRegistered;

            return Json(new
            {
                success = 1,
                NursingSummary = Helper.JsonPartialView(this, "_DataTemplate", model.NursingSummary),
                NursingLastRegistered = Helper.JsonPartialView(this, "_MiniReport", model.NursingLastRegistered),
                newday = 0,
                xmldate = _useXml ? Helper.GetXmlDate() : ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMemberDetails(int cardid, string surname)
        {
            var card = LastTenCards.FirstOrDefault(x => x.CardId == cardid && x.Surname == surname);

            if (card != null)
            {
                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewMember", card) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Charts

        private NursingSummaryInfo GetsummaryData()
        {
            var summary = new NursingSummaryInfo();

            if (!_useXml)
            {
                var today =  DateTime.Today;

                var year = new NursingProxy().NursingChannel.GetThisYear();
                var receiveddic = new NursingProxy().NursingChannel.GetReceivedThisYear();

                summary.ReceivedToday = receiveddic.Where(x => x.Key.Date == today).Sum(x => x.Value);
                summary.ReceivedThisWeek = receiveddic.Where(x => x.Key.Date >= today.StartOfWeek(DayOfWeek.Monday) 
                                                            && x.Key.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Sum(x => x.Value);
                summary.ReceivedThisMonth = receiveddic.Where(x => x.Key.Month == today.Month).Sum(x => x.Value);
                summary.ReceivedThisYear = receiveddic.Where(x => x.Key.Year == today.Year).Sum(x => x.Value);

                summary.CapturedToday = year.Count(x => x.DateCaptured.Date == today);
                summary.CapturedThisWeek = year.Count(x => x.DateCaptured.Date >= today.StartOfWeek(DayOfWeek.Monday) 
                                                    && x.DateCaptured.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6));
                summary.CapturedThisMonth = year.Count(x => x.DateCaptured.Month == today.Month);
                summary.CapturedThisYear = year.Count(x => x.DateCaptured.Year == today.Year);
                
                summary.QcToday = year.Count(x => x.DateQCd.Date == today);
                summary.QcThisWeek = year.Count(x => x.DateQCd.Date >= today.StartOfWeek(DayOfWeek.Monday) 
                                        && x.DateQCd.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6));
                summary.QcThisMonth = year.Count(x => x.DateQCd.Month == today.Month);
                summary.QcThisYear = year.Count(x => x.DateQCd.Year == today.Year);

                summary.PrintedToday = year.Count(x => x.PrintDate.Date == today);
                summary.PrintedThisWeek = year.Count(x => x.PrintDate.Date >= today.StartOfWeek(DayOfWeek.Monday) 
                                        && x.PrintDate.Date <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6));
                summary.PrintedThisMonth = year.Count(x => x.PrintDate.Month == today.Month);
                summary.PrintedThisYear = year.Count(x => x.PrintDate.Year == today.Year);
            }
            else
            {
                summary = Helper.GetSummaryInfo(summary);
            }

            return summary;
        }

        private NursingOperationSummary GetOperationsSummaryData()
        {
            var summary = new NursingOperationSummary();

            var stats = !_useXml ? new NursingProxy().NursingChannel.GetOperationsStats() : Helper.GetOperationStats();

            summary.NotPrinted = stats["print"];
            summary.NotQCd = stats["qc"];
            summary.NotCaptured = stats["capture"];

            return summary;
        }

        private Highcharts GetAllNursingRegisteredMonth(DateTime month, bool isBig)
        {
            // Get data
            var regvscount = new NursingProxy().NursingChannel.GetNursingRegistrationsData(month, DateTime.MinValue, 1);

            // Define chart
            var chart = new ChartItem
            {
                ChartName = isBig ? "BigPshipRegistrations" : "PshipRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                HideExportButtons = !isBig,
                TitleStyle = "",
                Title = "Card Prints",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Month",
                YAxisTitle = "No. of cards",
                LegendLayout = Layouts.Vertical,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = -10,
                LegendY = 100,
                HideLegend = !isBig,
                LegendPadding = 20
            };

            // Get chart data
            var result = GetChartData(regvscount, chart);

            return result;
        }

        private Highcharts GetAllNursingRegisteredYear(bool isBig)
        {
            var report = new List<NursingReport>();

            // Get data
            if (!_useXml)
                report = new NursingProxy().NursingChannel.GetLastYear();

            // Define chart for big one
            _tempyearchart = new ChartItem
            {
                ChartName = "BigNursingRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = false,
                Title = "Cards Printed",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Month",
                YAxisTitle = "Cards Printed",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = 0,
                LegendY = 100,
                HideLegend = false,
                LegendPadding = 20
            };

            if (_saveTemp && !_useXml)
            {
                LastYearData = report;
            }

            // Define chart
            var chart = new ChartItem
            {
                ChartName = isBig ? "BigNursingRegistrations" : "NursingRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Card Prints" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Month",
                YAxisTitle = "Cards Printed",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            var result = GetTotalChartData(LastYearData, chart);

            return result;
        }

        private Highcharts GetAveragesYear(bool isBig)
        {
            // Define chart for big one
            _tempavgchart = new ChartItem
            {
                ChartName = "BigNursingOperationAvg",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = false,
                Title = "Operation Averages",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Month",
                YAxisTitle = "Hours",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = 0,
                LegendY = 100,
                HideLegend = false,
                LegendPadding = 20
            };

            // Define chart
            var chart = new ChartItem
            {
                ChartName = isBig ? "BigNursingOperationAvg" : "NursingOperationAvg",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Operation Averages" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Month",
                YAxisTitle = "Hours",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            var result = GetAverageChartData(chart);

            return result;
        }

        private Highcharts GetAllNursingRegisteredMonth(bool isBig)
        {
            var report = new List<NursingReport>();
            Chartdatamonth = new Dictionary<string, int>();

            // Define chart for big one
            _tempmonthchart = new ChartItem
            {
                ChartName = "BigNursingMonthRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = false,
                Title = "Cards Printed",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Day",
                YAxisTitle = "No. of cards",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = HorizontalAligns.Right,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = 0,
                LegendY = 100,
                HideLegend = false,
                LegendPadding = 20
            };

            // Define chart
            var chart = new ChartItem
            {
                ChartName = isBig ? "BigNursingMonthRegistrations" : "NursingMonthRegistrations",
                ChartSeriesType = ChartTypes.Line,
                MarginRight = 20,
                ClassName = "chart",
                TitleStyle = "",
                HideExportButtons = !isBig,
                Title = isBig ? "Card Prints" : "",
                SubtitleStyle = "display: 'none'",
                Subtitle = "",
                XAxisText = "Day",
                YAxisTitle = "No. of cards",
                LegendLayout = Layouts.Horizontal,
                LegendAlign = isBig ? HorizontalAligns.Right : HorizontalAligns.Center,
                LegendVerticalAlign = VerticalAligns.Top,
                LegendX = isBig ? 0 : -10,
                LegendY = isBig ? 100 : -5,
                HideLegend = !isBig,
                LegendPadding = isBig ? 20 : 5
            };

            // Get days in this month
            var year = DateTime.Today.Year;
            var month = DateTime.Today.Month;
            _captured = new Dictionary<string, int>();
            _qcd = new Dictionary<string, int>();
            _printed = new Dictionary<string, int>();
            _days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).Select(d => d).ToList();

            // Set dictionary default values
            foreach (var i in _days)
            {
                if (i <= DateTime.Today.Day)
                {
                    _captured.Add(i.ToString(), 0);
                    _qcd.Add(i.ToString(), 0);
                    _printed.Add(i.ToString(), 0);
                }
            }

            // Get data
            if (!_useXml)
            {
                report = new NursingProxy().NursingChannel.GetThisYear();
                
                // Get chart data for each
                foreach (var rep in report)
                {
                    if (rep.DateCaptured.Year == year && rep.DateCaptured.Month == month)
                        _captured[rep.DateCaptured.Day.ToString()]++;

                    if (rep.DateQCd.Year == year && rep.DateQCd.Month == month)
                        _qcd[rep.DateQCd.Day.ToString()]++;

                    if (rep.PrintDate.Year == year && rep.PrintDate.Month == month)
                        _printed[rep.PrintDate.Day.ToString()]++;
                }                
            }
            else
            {
                var dics = Helper.GetThisYearData();

                foreach (var key in _captured.Keys.ToList())
                {
                    if (!dics["captured"].ContainsKey(key))
                        dics["captured"].Add(key, 0);

                    _captured[key] = dics["captured"][key];
                }

                foreach (var key in _qcd.Keys.ToList())
                {
                    if (!dics["qcd"].ContainsKey(key))
                        dics["qcd"].Add(key, 0);

                    _qcd[key] = dics["qcd"][key];
                }

                foreach (var key in _printed.Keys.ToList())
                {
                    if (!dics["printed"].ContainsKey(key))
                        dics["printed"].Add(key, 0);

                    _printed[key] = dics["printed"][key];
                }
            }

            if (_saveTemp)
            {
                ThisMonthData = report;
            }

            return GetMonthChart(report, chart);
        }

        #endregion

        #region Operations

        private Highcharts GetMonthChart(List<NursingReport> report, ChartItem chart)
        {
            // Get series
            var z = 0;
            var ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (report.Count == 0 && !_useXml)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                var printseries = new List<int>();
                foreach (var innerval in _printed)
                {
                    printseries.Add(innerval.Value);
                }

                ser.Add("Printed", printseries);

                var capturedseries = new List<int>();
                foreach (var innerval in _captured)
                {
                    capturedseries.Add(innerval.Value);
                }

                ser.Add("Captured", capturedseries);

                var qcseries = new List<int>();
                foreach (var innerval in _qcd)
                {
                    qcseries.Add(innerval.Value);
                }

                ser.Add("QCd", qcseries);

                // Assign series
                series = new Series[ser.Keys.Count];
                foreach (var val in ser)
                {
                    series[z] = new Series {
                        PlotOptionsSeries = new PlotOptionsSeries
                        {
                            LineWidth = 3,
                            Color = _colours[z], Visible = true,
                            Marker = new PlotOptionsSeriesMarker
                            {
                                Enabled = true
                            }
                        },
                        Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Set x-axis data
            var xAxis = _days.Select(x => x.ToString()).ToArray();

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetTotalChartData(List<NursingReport> lastYearData, ChartItem chart)
        {
            var regvscount = new Dictionary<string, int>();
            var z = 0;

            // Get series
            var ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (lastYearData.Count == 0 && !_useXml)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;
                Dictionary<string, int> monthly;

                if (_useXml)
                {
                    monthly = Helper.GetTotalMonthlyChart();
                    regvscount = Helper.GetTotalChart();
                }
                else
                {
                    monthly = Helper.GetDictionaryFromList(lastYearData, 1, DateTime.Today.AddYears(-1), DateTime.Today);

                    // Get total
                    regvscount = Helper.GetTotalFromDictionary(monthly);
                }

                var enrolseries = new List<int>();
                foreach (var innerval in regvscount)
                {
                    enrolseries.Add(innerval.Value);
                }

                ser.Add("Registered", enrolseries);

                // monthly
                var monthlyseries = new List<int>();
                foreach (var innerval in monthly)
                {
                    monthlyseries.Add(innerval.Value);
                }

                ser.Add("Monthly", monthlyseries);

                // Assign series
                series = new Series[ser.Keys.Count];

                foreach (var val in ser)
                {
                    series[z] = new Series
                    {
                        PlotOptionsSeries = new PlotOptionsSeries
                        {
                            LineWidth = 3,
                            Color = _colours[z],
                            Visible = true,
                            Marker = new PlotOptionsSeriesMarker
                            {
                                Enabled = true
                            }
                        },
                        Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray()),
                        Type = val.Key == "Monthly" ? ChartTypes.Column : ChartTypes.Line
                    };

                    z++;
                }
            }

            // Set x-axis data
            var xAxis = regvscount.Keys.ToArray();

            // For Nursing dashboard
            for (var i = 0; i < xAxis.Length; i++)
                xAxis[i] = xAxis[i].Split(' ')[0];

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetAverageChartData(ChartItem chart)
        {
            var z = 0;

            // Get series
            var ser = new Dictionary<string, List<int>>();

            Series[] series;
            var start = DateTime.Today.AddYears(-1);
            var end = DateTime.Today;

            if (LastYearData.Count == 0 && !_useXml)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                var avgs = _useXml ? Helper.GetOperationAverages() : new NursingProxy().NursingChannel.GetOperationAverages(start, end);

                var qc = avgs["qcavg"];
                var print = avgs["printavg"];
                var capture = avgs["captureavg"];
                var total = avgs["totalavg"];

                var captureseries = new List<int>();
                foreach (var innerval in capture)
                {
                    captureseries.Add(innerval.Value);
                }

                var qcseries = qc.Select(innerval => innerval.Value).ToList();

                var printseries = print.Select(innerval => innerval.Value).ToList();

                var totalseries = total.Select(innerval => innerval.Value).ToList();

                ser.Add("QC Avg", qcseries);
                ser.Add("Capture Avg", captureseries);
                ser.Add("Print Avg", printseries);
                ser.Add("Total Avg", totalseries);

                // Assign series
                series = new Series[ser.Keys.Count];

                foreach (var val in ser)
                {
                    series[z] = new Series
                    {
                        PlotOptionsSeries = new PlotOptionsSeries
                        {
                            LineWidth = 3,
                            Color = _colours[z],
                            Visible = true,
                            Marker = new PlotOptionsSeriesMarker
                            {
                                Enabled = true
                            }
                        },
                        Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray()),
                        Type = ChartTypes.Line
                    };

                    z++;
                }
            }

            // Set x-axis data
            var monthlist = new List<string>();
            const string format = "MMM yy";

            for (var d = start; d <= end; d = d.AddMonths(1))
            {
                monthlist.Add(d.ToString(format));
            }

            var xAxis = monthlist.ToArray();

            // For Nursing dashboard
            for (var i = 0; i < xAxis.Length; i++)
                xAxis[i] = xAxis[i].Split(' ')[0];

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts GetChartData(Dictionary<string, int> regvscount, ChartItem chart)
        {
            var z = 0;

            // Get series
            var ser = new Dictionary<string, List<int>>();
            Series[] series;

            if (regvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                chart.HideLegend = true;
            }
            else
            {
                chart.HideLegend = false;

                var enrolseries = new List<int>();
                foreach (var innerval in regvscount)
                {
                    enrolseries.Add(innerval.Value);
                }

                ser.Add("Registered", enrolseries);

                // Assign series
                series = new Series[ser.Keys.Count];
                foreach (var val in ser)
                {
                    series[z] = new Series
                    {
                        PlotOptionsSeries = new PlotOptionsSeries
                        {
                            LineWidth = 3,
                            Color = _colours[z],
                            Visible = true,
                            Marker = new PlotOptionsSeriesMarker
                            {
                                Enabled = true
                            }
                        }, Name = val.Key,
                        Data = new Data(val.Value.Cast<object>().ToArray())
                    };
                    z++;
                }
            }

            // Set x-axis data
            var xAxis = regvscount.Keys.ToArray();

            // For Nursing dashboard
            for (var i = 0; i < xAxis.Length; i++)
                xAxis[i] = xAxis[i].Split(' ')[0];

            return CreateChart(xAxis, series, chart);
        }

        private Highcharts CreateChart(string[] timespan, Series[] series, ChartItem item)
        {
            var chart = new Highcharts(item.ChartName)
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = item.ChartSeriesType,
                    MarginRight = item.MarginRight,
                    ClassName = item.ClassName
                })
                .SetTitle(new Title { Style = item.TitleStyle, Text = item.Title })
                .SetSubtitle(new Subtitle { Style = item.SubtitleStyle, Text = item.Subtitle })
                .SetXAxis(new XAxis {
                    Categories = timespan,
                    Min = 0,
                    Labels = new XAxisLabels
                    {
                        StaggerLines = 1,
                        Step = 2
                    },
                    Max = timespan.Length - 1,
                    StartOnTick = true,
                    Title = new XAxisTitle
                    {
                        Text = item.XAxisText
                    }
                })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle
                    {
                        Text = item.YAxisTitle
                    }
                })
                .SetExporting(new Exporting
                {
                    Enabled = !item.HideExportButtons
                })
                .SetLegend(new Legend
                {
                    Layout = item.LegendLayout,
                    Align = item.LegendAlign,
                    VerticalAlign = item.LegendVerticalAlign,
                    X = item.LegendX,
                    Y = item.LegendY,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !item.HideLegend,
                    Padding = item.LegendPadding,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine
                    {
                        Marker = new PlotOptionsLineMarker
                        {
                            Enabled = true, Symbol = "circle"
                        }
                    }
                })
                .SetSeries(series);

            return chart;
        }

        #endregion
    }
}