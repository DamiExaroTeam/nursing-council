﻿using System;
using System.Globalization;
using System.IO;
using System.Web.Mvc;

namespace NursingDashboard.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var errordir = Path.Combine("C:", "ErrorLogs");

            if (!Directory.Exists(errordir))
                Directory.CreateDirectory(errordir);

            string filePath = Path.Combine(errordir, DateTime.Now.ToString("yyyy-MM-dd") + ".txt");

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(DateTime.Now.ToString("hh:mmtt"));
                writer.WriteLine("==============");
                writer.WriteLine("Message :" + filterContext.Exception.Message + "<br/>" + Environment.NewLine + "StackTrace :" + filterContext.Exception.StackTrace +
                                    "" + Environment.NewLine + "Date :" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }


            //Helper.LogError(filterContext.Exception, Request.Url?.ToString(), User.Identity.GetUserId());

            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error.cshtml"
            };

            if (Request.IsAjaxRequest())
                filterContext.Result = Json(new
                {
                    success = 0
                }, JsonRequestBehavior.AllowGet);

            filterContext.ExceptionHandled = true;
        }
    }
}