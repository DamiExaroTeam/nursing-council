﻿using Nursing_Common;
using NursingDashboard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace NursingDashboard.Helpers
{
    public static class Helper
    {
        public static string Nursingxml = ConfigurationManager.AppSettings["nursingxml"];

        public static string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        private static int _seed = Environment.TickCount;

        private static readonly ThreadLocal<Random> RandomWrapper = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref _seed)));

        public static string GenerateRandomColour()
        {
            return $"#{RandomWrapper.Value.Next(0x1000000):X6}";
        }

        public static string CapitalizeAfter(this string s, IEnumerable<char> chars)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            var charsHash = new HashSet<char>(chars);
            var sb = new StringBuilder(s.ToLower());

            // Capitalize first letter
            sb[0] = char.ToUpper(sb[0]);

            for (var i = 0; i < sb.Length - 1; i++)
            {
                if (charsHash.Contains(sb[i]))
                    sb[i + 1] = char.ToUpper(sb[i + 1]);
            }
            return sb.ToString();
        }

        public static string ToFirstLetterUpperSentence(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var words = input.ToLower().Split(' ');
                var result = string.Empty;

                foreach (var word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        var a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }
       
        public static string ToFirstLetterUpper(string word)
        {
            if (word != string.Empty && word != " ")
            {
                var a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }

        public static Dictionary<string, int> GetDictionaryFromList(List<NursingReport> reports, int type, DateTime start, DateTime end)
        {
            var result = new Dictionary<string, int>();

            // Set defaults
            if (type == 1)
            {
                var months = MonthsBetween(start, end);
                foreach (var s in months)
                {
                    result.Add(s.Item1, 0);
                }
            }

            foreach (var rep in reports)
            {
                var date = rep.PrintDate.ToString(type == 1 ? "MMM yy" : "dd");

                if (result.ContainsKey(date))
                {
                    result[date]++;
                }
                else
                    result.Add(date, 1);
            }

            return result;
        }

        public static Dictionary<string, int> GetTotalFromDictionary(Dictionary<string, int> dic)
        {
            var counter = 0;
            var result = new Dictionary<string, int>(dic);

            var keys = new List<string>(result.Keys);

            foreach (var key in keys)
            {
                counter += result[key];
                result[key] = counter;
            }
            return result;
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static IEnumerable<Tuple<string>> MonthsBetween(DateTime startDate, DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;

            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }

            while (iterator <= limit)
            {
                yield return Tuple.Create(iterator.ToString("MMM yy"));
                iterator = iterator.AddMonths(1);
            }
        }

        private static Dictionary<string, int> GetDictionaryFromXml(string dic)
        {
            var result = new Dictionary<string, int>();
            var key = string.Empty;
            var value = string.Empty;
            var capturingkey = false;
            var capturingvalue = false;

            foreach (var s in dic)
            {
                if (s == ']')
                {
                    if (capturingvalue)
                    {
                        result[key] = int.Parse(value);
                        capturingvalue = false;
                        key = string.Empty;
                        value = string.Empty;
                    }
                }

                if (capturingvalue)
                    value += s;

                if (s == ',')
                {
                    result.Add(key, 0);
                    capturingkey = false;
                    capturingvalue = true;
                }

                if (capturingkey)
                    key += s;

                if (s == '[')
                {
                    capturingkey = true;
                }
            }

            return result;
        }       

        #region XML Requests

        public static Dictionary<string, int> GetTotalChart()
        {
            var doc = XDocument.Load(Nursingxml);
            var dic = doc.Root?.Element("TotalChartData")?.Value;
            return GetDictionaryFromXml(dic);
        }

        public static Dictionary<string, int> GetTotalMonthlyChart()
        {
            var doc = XDocument.Load(Nursingxml);
            var dic = doc.Root?.Element("TotalMonthyChartData")?.Value;
            return GetDictionaryFromXml(dic);
        }

        public static Dictionary<string, int> GetMonthChartData()
        {
            var doc = XDocument.Load(Nursingxml);
            var dic = doc.Root?.Element("MonthChartData")?.Value;
            return GetDictionaryFromXml(dic);
        }

        public static string GetXmlDate()
        {
            var doc = XDocument.Load(Nursingxml);
            return doc.Root?.Element("XMLDate")?.Value;
        }

        public static Dictionary<string, Dictionary<string, int>> GetThisYearData()
        {
            var doc = XDocument.Load(Nursingxml);

            var captured = GetDictionaryFromXml(doc.Root?.Element("MonthChartCapturedData")?.Value);
            var qcd = GetDictionaryFromXml(doc.Root?.Element("MonthChartQCdData")?.Value);
            var printed = GetDictionaryFromXml(doc.Root?.Element("MonthChartPrintedData")?.Value);

            var result =
                new Dictionary<string, Dictionary<string, int>>
                {
                    {"captured", captured},
                    {"qcd", qcd},
                    {"printed", printed}
                };

            return result;
        }

        public static Dictionary<string, Dictionary<string, int>> GetOperationAverages()
        {
            var result = new Dictionary<string, Dictionary<string, int>>();
            var doc = XDocument.Load(Nursingxml);
            var qcavg = doc.Root?.Element("Averages")?.Element("qcavg")?.Value;
            var pavg = doc.Root?.Element("Averages")?.Element("printavg")?.Value;
            var cavg = doc.Root?.Element("Averages")?.Element("captureavg")?.Value;
            var tavg = doc.Root?.Element("Averages")?.Element("totalavg")?.Value;

            result.Add("qcavg", GetDictionaryFromXml(qcavg));
            result.Add("printavg", GetDictionaryFromXml(pavg));
            result.Add("captureavg", GetDictionaryFromXml(cavg));
            result.Add("totalavg", GetDictionaryFromXml(tavg));
            return result;
        }

        public static NursingSummaryInfo GetSummaryInfo(NursingSummaryInfo summary)
        {
            var doc = XDocument.Load(Nursingxml);
            var dic = doc.Root?.Element("SummaryData")?.Value;
            var sum = GetDictionaryFromXml(dic);

            summary.CapturedToday = sum["CapturedToday"];
            summary.CapturedThisWeek = sum["CapturedThisWeek"];
            summary.CapturedThisMonth = sum["CapturedThisMonth"];
            summary.CapturedThisYear = sum["CapturedThisYear"];

            summary.ReceivedToday = sum["ReceivedToday"];
            summary.ReceivedThisWeek = sum["ReceivedThisWeek"];
            summary.ReceivedThisMonth = sum["ReceivedThisMonth"];
            summary.ReceivedThisYear = sum["ReceivedThisYear"];

            summary.QcToday = sum["QCToday"];
            summary.QcThisWeek = sum["QCThisWeek"];
            summary.QcThisMonth = sum["QCThisMonth"];
            summary.QcThisYear = sum["QCThisYear"];

            summary.PrintedToday = sum["PrintedToday"];
            summary.PrintedThisWeek = sum["PrintedThisWeek"];
            summary.PrintedThisMonth = sum["PrintedThisMonth"];
            summary.PrintedThisYear = sum["PrintedThisYear"];

            return summary;
        }

        public static List<NursingReport> GetLastTenRegistered()
        {
            var result = new List<NursingReport>();
            var doc = XDocument.Load(Nursingxml);

            foreach (var xe in doc.Descendants("Record"))
            {
                result.Add(Deserialize<NursingReport>(xe.Value));
            }

            // Ensure we are looking at today's data otherwise show empty
            //for (int i = 0; i < result.Count; i++)
            //{
            //    if (result[i].PrintDate.Date != DateTime.Today)
            //    {
            //        result.RemoveAt(i);
            //    }
            //}

            return result;
        }

        public static Dictionary<string, int> GetOperationStats()
        {
            var doc = XDocument.Load(Nursingxml);
            var dic = doc.Root?.Element("OperationStats")?.Value;
            return GetDictionaryFromXml(dic);
        }

        public static T Deserialize<T>(string xmlText)
        {
            if (String.IsNullOrWhiteSpace(xmlText)) return default(T);

            using (var stringReader = new StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }
        #endregion
    }

    public static class ListExtenstions
    {
        public static void AddMany<T>(this List<T> list, params T[] elements)
        {
            list.AddRange(elements);
        }

        public static void AddRange<T, TS>(this Dictionary<T, TS> source, Dictionary<T, TS> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException($"Collection is null");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
            }
        }

        
    }
}