﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace UpdateNursingService
{
    public class NursingService : UpdateNursingContract
    {
        public string UpdateDashboard(string xml)
        {
            try
            {
                string location = "C:\\NursingDashboard\\NursingDashboard.xml";

                // Save xml
                XDocument doc = XDocument.Parse(xml);
                doc.Save(location);

                return string.Empty;
            }
            catch (Exception ex)
            {
                // log error if needed
                return ex.Message;
            }
        }
    }
}
