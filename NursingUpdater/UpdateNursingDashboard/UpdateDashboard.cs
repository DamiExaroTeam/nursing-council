﻿using Nursing_Common;
using Nursing_Proxy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using UpdateNursingService;

namespace UpdateNursingDashboard
{
    public partial class UpdateDashboard : ServiceBase
    {
        public UpdateDashboard()
        {
            InitializeComponent();
        }

        // Basically, do all queries, create an xml and pass it to cloud
        protected override void OnStart(string[] args)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("NursingDashboardData");

            // TotalChart
            List<NursingReport> LastYearData = new NursingProxy().NursingChannel.GetLastYear();
            Dictionary<string, int> monthly = GetDictionaryFromList(LastYearData, 1, DateTime.Today.AddYears(-1), DateTime.Today);
            Dictionary<string, int> regvscount = GetTotalFromDictionary(monthly);
            root.Add(new XElement("TotalMonthyChartData", monthly));
            root.Add(new XElement("TotalChartData", regvscount));

            // Month report
            List<NursingReport> report = new NursingProxy().NursingChannel.GetThisYear();

            int year = DateTime.Today.Year;
            int month = DateTime.Today.Month;
            int day = DateTime.Today.Day;
            Dictionary<string, int> captured = new Dictionary<string, int>();
            Dictionary<string, int> qcd = new Dictionary<string, int>();
            Dictionary<string, int> printed = new Dictionary<string, int>();

            List<int> days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).Select(d => d).ToList();

            foreach (int i in days)
            {
                captured.Add(i.ToString(), 0);
                qcd.Add(i.ToString(), 0);
                printed.Add(i.ToString(), 0);
            }

            // Get chart data for each
            foreach (NursingReport rep in report)
            {
                if (rep.DateCaptured.Year == year && rep.DateCaptured.Month == month)
                    captured[rep.DateCaptured.Day.ToString()]++;

                if (rep.DateQCd.Year == year && rep.DateQCd.Month == month)
                    qcd[rep.DateQCd.Day.ToString()]++;

                if (rep.PrintDate.Year == year && rep.PrintDate.Month == month)
                    printed[rep.PrintDate.Day.ToString()]++;
            }

            root.Add(new XElement("MonthChartCapturedData", captured));
            root.Add(new XElement("MonthChartQCdData", qcd));
            root.Add(new XElement("MonthChartPrintedData", printed));

            // Operation averages
            Dictionary<string, Dictionary<string, int>> avgs = new NursingProxy().NursingChannel.GetOperationAverages(DateTime.Today.AddMonths(-12), DateTime.Today);
            XElement averages = new XElement("Averages");
            foreach (KeyValuePair<string, Dictionary<string,int>> pair in avgs)
            {
                averages.Add(new XElement(pair.Key, pair.Value));
            }

            root.Add(averages);

            // Operation stats
            Dictionary<string, int> stats = new NursingProxy().NursingChannel.GetOperationsStats();
            root.Add(new XElement("OperationStats", stats));

            // Summary data
            Dictionary<string, int> summary = new Dictionary<string, int>();
            DateTime today = DateTime.Today;
            List<NursingReport> yr = new NursingProxy().NursingChannel.GetThisYear();

            summary.Add("CapturedToday", yr.Where(x => x.DateCaptured == today).Count());
            summary.Add("CapturedThisWeek", yr.Where(x => x.DateCaptured >= today.StartOfWeek(DayOfWeek.Monday) && x.DateCaptured <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count());
            summary.Add("CapturedThisMonth", yr.Where(x => x.DateCaptured.Month == today.Month).Count());
            summary.Add("CapturedThisYear", yr.Where(x => x.DateCaptured.Year == today.Year).Count());

            summary.Add("ReceivedToday", yr.Where(x => x.DateReceived == today).Count());
            summary.Add("ReceivedThisWeek", yr.Where(x => x.DateReceived >= today.StartOfWeek(DayOfWeek.Monday) && x.DateReceived <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count());
            summary.Add("ReceivedThisMonth", yr.Where(x => x.DateReceived.Month == today.Month).Count());
            summary.Add("ReceivedThisYear", yr.Where(x => x.DateReceived.Year == today.Year).Count());

            summary.Add("QCToday", yr.Where(x => x.DateQCd == today).Count());
            summary.Add("QCThisWeek", yr.Where(x => x.DateQCd >= today.StartOfWeek(DayOfWeek.Monday) && x.DateQCd <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count());
            summary.Add("QCThisMonth", yr.Where(x => x.DateQCd.Month == today.Month).Count());
            summary.Add("QCThisYear", yr.Where(x => x.DateQCd.Year == today.Year).Count());

            summary.Add("PrintedToday", yr.Where(x => x.PrintDate == today).Count());
            summary.Add("PrintedThisWeek", yr.Where(x => x.PrintDate >= today.StartOfWeek(DayOfWeek.Monday) && x.PrintDate <= today.StartOfWeek(DayOfWeek.Monday).AddDays(6)).Count());
            summary.Add("PrintedThisMonth", yr.Where(x => x.PrintDate.Month == today.Month).Count());
            summary.Add("PrintedThisYear", yr.Where(x => x.PrintDate.Year == today.Year).Count());

            root.Add(new XElement("SummaryData", summary));

            // Last10Records
            List<NursingReport> lastrecs = new NursingProxy().NursingChannel.GetLastTenRegistered();
            XElement records = new XElement("Records");
            foreach (NursingReport rep in lastrecs)
            {
                records.Add(new XElement("Record", rep.SerializeObject<NursingReport>()));
            }

            root.Add(new XElement("LastTenRecords", records));

            doc.Add(root);

            // Save document in cloud
            string error = new UpdatingProxy().NursingChannel.UpdateDashboard(doc.ToString());

            doc.Save("C://NursingXml.xml");

            if (!string.IsNullOrEmpty(error))
            {
                // log error somewhere
                string log = error;
            }
        }

        protected override void OnStop()
        {

        }

        #region Operations

        public Dictionary<string, int> GetDictionaryFromList(List<NursingReport> reports, int type, DateTime start, DateTime end)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            string date = string.Empty;

            // Set defaults
            if (type == 1)
            {
                var months = MonthsBetween(start, end);
                foreach (Tuple<string> s in months)
                {
                    result.Add(s.Item1, 0);
                }
            }

            foreach (NursingReport rep in reports)
            {
                if (type == 1)
                    date = rep.PrintDate.ToString("MMM yy");
                else
                    date = rep.PrintDate.ToString("dd");

                if (result.ContainsKey(date))
                {
                    result[date]++;
                }
                else
                    result.Add(date, 1);
            }

            return result;
        }

        public Dictionary<string, int> GetTotalFromDictionary(Dictionary<string, int> dic)
        {
            int counter = 0;
            Dictionary<string, int> result = new Dictionary<string, int>(dic);

            var keys = new List<string>(result.Keys);

            foreach (string key in keys)
            {
                counter += result[key];
                result[key] = counter;
            }
            return result;
        }

        public IEnumerable<Tuple<string>> MonthsBetween(DateTime startDate, DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;

            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }

            var dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            while (iterator <= limit)
            {
                yield return Tuple.Create(iterator.ToString("MMM yy"));
                iterator = iterator.AddMonths(1);
            }
        }
        #endregion
    }

    public static class Extensions
    {
        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
    }
}
