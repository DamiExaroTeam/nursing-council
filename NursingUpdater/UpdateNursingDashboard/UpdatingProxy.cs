﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace UpdateNursingService
{
    public class UpdatingProxy
    {
        private ChannelFactory<UpdateNursingContract> cf = (ChannelFactory<UpdateNursingContract>)null;

        public UpdateNursingContract NursingChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public UpdatingProxy()
        {
            this.ServiceBaseAddress = ConfigurationManager.AppSettings["UpdateServiceUrl"].ToString() + "/UpdateNursing.svc";
            WSHttpBinding wsHttpBinding = new WSHttpBinding();
            wsHttpBinding.MaxReceivedMessageSize = 4194304L;
            wsHttpBinding.Security.Mode = SecurityMode.Transport;
            wsHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            wsHttpBinding.SendTimeout = new TimeSpan(0, 10, 0);
            wsHttpBinding.ReceiveTimeout = new TimeSpan(0, 10, 0);
            wsHttpBinding.MaxReceivedMessageSize = (long)int.MaxValue;
            wsHttpBinding.ReaderQuotas.MaxArrayLength = 4194304;
            ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)((param0, param1, param2, param3) => true);
            this.cf = new ChannelFactory<UpdateNursingContract>((Binding)wsHttpBinding, this.ServiceBaseAddress);
            this.NursingChannel = this.cf.CreateChannel();
        }
    }
}
