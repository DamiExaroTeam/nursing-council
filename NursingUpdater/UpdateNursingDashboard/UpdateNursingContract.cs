﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace UpdateNursingService
{
    [ServiceContract]
    public interface UpdateNursingContract
    {
        [OperationContract]
        string UpdateDashboard(string xml);
    }
}
